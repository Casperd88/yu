import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import AuthManager from './components/AuthManager';
import { ThemeProvider } from 'styled-components';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import rootReducer from './reducers';
import App from './App';
import theme from './theme';

const store = createStore(rootReducer, {}, applyMiddleware(logger, thunk));

export default () => (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <AuthManager>
        <App />
      </AuthManager>
    </ThemeProvider>
  </Provider>
);
