export default {
  colors: {
    primary: '#10ad9e',
    secondary: '#f1592f',
    error: '#ff3860',
    warning: '#fbb22c',
    info: '#58615f',
    success: '#4ed25e',
    notification: '#ff3860',
    text: '#828c8a',
    bold: '#4b6361',
    muted: '#c7cecc',
    accent: '#ecf3f2',
    darkgrey: '#58615f',
    white: '#fff',
    lightgrey: '#b3bfbf'
  },
  boxShadow: '0 1px 3px rgba(0,0,0,0.08)',
  boxShadowLarge: '0 0.2em 0.6em rgba(44, 53, 53, 0.1)',
  defaultFont: "'Stolzl', sans-serif",
}

/*
export default {
  colors: {
    primary: '#2f3837',
    secondary: '#2f3837',
    error: '#ff3860',
    text: '#2f3837',
    bold: '#2f3837',
    muted: '#2f3837',
    accent: '#2f3837',
    darkgrey: '#2f3837',
    white: '#2f3837',
  },
  boxShadow: '0 0 0 4px #2f3837',
}
*/
