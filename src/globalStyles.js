import { injectGlobal } from 'styled-components';
import { darken } from 'polished';
import theme from './theme';

injectGlobal`
@font-face {
  font-family: 'Stolzl';
  src: url('/fonts/stolzl/Stolzl-Light.woff') format('woff');
  font-weight: 300;
  font-style: normal;
}

@font-face {
  font-family: 'Stolzl';
  src: url('/fonts/stolzl/Stolzl-Regular.woff') format('woff');
  font-weight: normal;
  font-style: normal;
}

@font-face {
  font-family: 'Stolzl';
  src: url('/fonts/stolzl/Stolzl-Medium.woff') format('woff');
  font-weight: 500;
  font-style: normal;
}

@font-face {
  font-family: 'Stolzl';
  src: url('/fonts/stolzl/Stolzl-Medium.woff') format('woff');
  font-weight: 700;
  font-style: normal;
}

html {
  touch-action: manipulation;
}

strong {
  font-weight: 500;
}

html, body {
  height: 100%;
  background: #ecf3f2;
}

body {
  background: #ecf3f2;
  font-family: 'Stolzl', sans-serif;
  font-weight: 400;
  color: ${theme.colors.text};
  font-size: 16px;
  padding: 4.8em 0 0 0;
  margin: 0;
  line-height: 1.6em;
}

* {
  box-sizing: border-box;
}

h1, h2, h3, h4, h5, h6 {
  font-weight: normal;
}

img {
  max-width: 100%;
  height: auto;
}

a {
  cursor: pointer;
  text-decoration: none;
  color: ${theme.colors.primary};
  &:hover {
    color: ${darken(0.1, theme.colors.primary)};
  }
}

strong {
  font-weight: normal;
  color: ${theme.colors.bold};
}
`;



















/*
  .box {
    position: relative;
  }
  .box .progress {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
  }

  .section-title {
    font-size: 1.2em;
    margin: 0 0 1.2em 0;
    font-weight: normal;
  }

  #homepage {
  background-image: url('http://luxuryibizayachts.com/wp-content/uploads/2015/06/BAN35-1024x513.jpg');
  background-size: cover;
  background-position: center top;
  box-shadow: inset 0 0 0 10000px rgba(26, 45, 41, 0.65);
  overflow: hidden;
  position: relative;
  margin-top: -80px
}

#homepage .hero-body {
  padding: 0;
  height:40vw;
  display: flex;
  align-items: center;
  justify-content: center;
}

.box {
  border-radius: 0;
  border: none;
}

.card-content {
  padding: 1rem;
}

.input, .textarea {
  border-radius: 0;
  box-shadow: none;
  border: 0;
  border-bottom: 1px #ddd solid;
}
.input:focus, .textarea:focus,
.input.is-danger:focus, .textarea.is-danger:focus {
  box-shadow: none;
}
.main-search {
  width: 40vw !important;
}

body .navbar {
  box-shadow: 0 1px 3px rgba(0,0,0,0.08);
}

body .navbar.is-dark {
  box-shadow: none;
}

body .navbar.is-dark .navbar-item.logo,
body .navbar .navbar-item.logo  {
  display: inline-flex;
  height: 3em;
  width: 3em;
  margin: 0.4em;
  font-size: 1.4em;
  justify-content: center;
  align-items: center;
  transform: rotate(10deg);
  transition: transform 150ms ease-in-out;
  color: #00d1b2;
}
body .navbar.is-dark .navbar-item.logo {
  color: rgba(255, 255, 255, 0.5);
}
body .navbar .navbar-item.logo:hover {
  background: none;
}
.logo:active,
.logo:focus {
  transform: scale(0.8);
}

h1.title.is-1 {
  text-transform: uppercase;
  font-size: 2em;
  letter-spacing: 0.10em;
  font-weight: 500;
}



.muted {
  color: rgba(0,0,0,.5);
  display: block;
  font-weight: normal !important;
}

body .tag {
  white-space: normal !important;
  height: auto;
  padding: 0 1rem;
  min-height: 2em;
  line-height: 2em;
  border-radius: 2em;
  margin: 1em;
}



.navbar-item, .navbar-link, .box {
  color: #3e4a4a;
}
.button.is-secondary {
  background: #ef6b4d;
  color: white;
  border-color: transparent;
}

.progress {
  border-radius: 0;
}
.progress.is-small {
  height: 8px;
}

.notifications {
  margin-bottom: 2em;
}

.fade-enter {
  opacity: 0.01;
}

.fade-enter-active {
  opacity: 1;
  transition: opacity 1s;
}

.fade-exit {
  opacity: 1;
  display: none;
}

.fade-exit-active {
  opacity: 0;
  transition: opacity 0.5s;
}


body .navbar {
  transition: background-color 300ms ease-in;
  height: 80px;
  overflow: hidden;
}
body .navbar.is-dark {
  background: transparent;
}

body .navbar.overlay {
  background: white;
}
*/
