import {notify} from './notification';
import firebase from 'firebase';

/*const LOGIN           = 'user/LOGIN';
const LOGIN_SUCCESS   = 'user/LOGIN_SUCCESS';
const RESET_PASSWORD  = 'user/RESET_PASSWORD';
*/

export const authSuccess = (user) => ({
  type: 'AUTH_SUCCESS',
  payload: user
});

export const authLogin = (email, password, persist = 'LOCAL') => {
  return async (dispatch) => {
    return new Promise(async (resolve, reject) => {
      try {
        await firebase.auth().setPersistence(
          firebase.auth.Auth.Persistence[persist ? 'LOCAL' : 'SESSION']
        );
        const user = firebase.auth().signInWithEmailAndPassword(
          email,
          password
        );
        resolve(user)
      } catch (error) {
        reject(error)
      }
    })

  };
};

export const resetPassword = (email) => {
  return (dispatch) => {
    return firebase.auth().sendPasswordResetEmail(email).then(function() {
      dispatch(notify(
        'Instructions sent',
        'An e-mail with instructions on how to reset your password has been sent to ' + email,
        'info'
      ))
    }).catch(function(error) {
      console.log('error!', error)
      dispatch(notify(error.code, error.message))
    });
  }
}

export const authLogout = () => {
  return (dispatch) => {
    return firebase
      .auth()
        .signOut()
            .catch(error => dispatch(notify(error.message)));
  };
};

export const authLogoutSuccess = () => ({
  type: 'AUTH_LOGOUT',
});
