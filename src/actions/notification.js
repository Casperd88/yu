export const notify = (title, message = false, type  = 'error') => {
  return (dispatch) => {
    const $id = (new Date()).getTime();
    dispatch({
      type: 'NOTIFY',
      payload: {$id, title, message, type}
    });
    setTimeout(() => {
      dispatch(removeNotification($id));
    }, 10000)
  }
};

export const removeNotification = ($id) => ({
  type: 'REMOVE_NOTIFY',
  payload: $id
});

export const purgeNotifications = () => ({
  type: 'PURGE_NOTIFY'
});
