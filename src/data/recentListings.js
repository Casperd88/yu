export default [
  {
    photos: [
      '/images/listings/boat.jpg',
      '/images/listings/monte.jpg',
      '/images/listings/64.jpg'
    ],
    title: 'Bavaria Cruiser 46',
    price: 285000,
    year: 2014,
    location: 'Marbella, Andalucia',
    length: 47,
    brokerName: 'Navi-Sell',
    brokerLogo: '/images/brokers/navisel.jpg'
  },
  {
    photos: ['/images/listings/monte.jpg','/images/listings/boat.jpg',
    '/images/listings/monte.jpg',
    '/images/listings/64.jpg'],
    title: 'Beneteau Monte Carlo 5',
    price: 765000,
    year: 2017,
    location: 'Marbella, Andalucia',
    length: 38,
    brokerName: 'Navi-Sell',
    brokerLogo: '/images/brokers/navisel.jpg'
  },
  {
    photos: ['/images/listings/64.jpg','/images/listings/boat.jpg',
    '/images/listings/monte.jpg',
    '/images/listings/64.jpg'],
    title: 'Jeanneau 64',
    price: 1140000,
    year: 2014,
    location: 'Malaga, Andalucia',
    length: 47,
    brokerName: 'Navi-Sell',
    brokerLogo: '/images/brokers/navisel.jpg'
  },
  {
    photos: [
      '/images/listings/boat.jpg',
      '/images/listings/monte.jpg',
      '/images/listings/64.jpg'
    ],
    title: 'Bavaria Cruiser 46',
    price: 285000,
    year: 2014,
    location: 'Marbella, Andalucia',
    length: 47,
    brokerName: 'Navi-Sell',
    brokerLogo: '/images/brokers/navisel.jpg'
  },
  {
    photos: ['/images/listings/monte.jpg','/images/listings/boat.jpg',
    '/images/listings/monte.jpg',
    '/images/listings/64.jpg'],
    title: 'Beneteau Monte Carlo 5',
    price: 765000,
    year: 2017,
    location: 'Marbella, Andalucia',
    length: 38,
    brokerName: 'Navi-Sell',
    brokerLogo: '/images/brokers/navisel.jpg'
  },
  {
    photos: ['/images/listings/64.jpg','/images/listings/boat.jpg',
    '/images/listings/monte.jpg',
    '/images/listings/64.jpg'],
    title: 'Jeanneau 64',
    price: 1140000,
    year: 2014,
    location: 'Malaga, Andalucia',
    length: 47,
    brokerName: 'Navi-Sell',
    brokerLogo: '/images/brokers/navisel.jpg'
  },
];
