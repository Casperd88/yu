export default [
  {
    image: '/images/costas/almeria.jpg',
    name: 'Almeria'
  },
  {
    image: '/images/costas/azahar.jpg',
    name: 'Azahar'
  },
  {
    image: '/images/costas/blanca.jpg',
    name: 'Blanca'
  },
  {
    image: '/images/costas/brava.jpg',
    name: 'Brava'
  },
  {
    image: '/images/costas/calida.jpg',
    name: 'Calida'
  },

  {
    image: '/images/costas/cantabria.jpg',
    name: 'Cantabria'
  },
  {
    image: '/images/costas/dorada.jpg',
    name: 'Dorada'
  },
  {
    image: '/images/costas/delaluz.jpg',
    name: 'Luz'
  },
  /*
  {
    image: '',
    name: 'Costa de Marisco'
  },*/
  {
    image: '/images/costas/delsol.jpg',
    name: 'Sol'
  },
  {
    image: '/images/costas/tropical.jpg',
    name: 'Tropical'
  },
  {
    image: '/images/costas/vasca.jpg',
    name: 'Vasca'
  },
  {
    image: '/images/costas/verde.jpg',
    name: 'Verde'
  }
];
