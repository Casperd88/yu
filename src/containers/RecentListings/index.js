import React, { Component } from 'react';
import ListCard from './../../components/ListCard';
import Carousel from './../../components/Carousel';
import recentListings from '../../data/recentListings';

export default class RecentListings extends Component {

  componentWillMount() {
    this.setState({
      listings: recentListings
    });
  }

  render() {
    return (
      <Carousel items={{
        mobile: 1,
        landscape: 2,
        tablet: 2,
        desktop: 3
      }} space={1}>
        {this.state.listings.map((listing, index) => (
          <ListCard key={index} {...listing} />
        ))}
      </Carousel>
    );
  }
}
