import React, { Component } from 'react';
import PhotoCard from './../../components/PhotoCard';
import Carousel from './../../components/Carousel';
import listOfCostas from '../../data/costas';

const Costa = ({image, name}) => (
  <PhotoCard image={image} label={name} link="/"  />
);

export default class Costas extends Component {

  componentWillMount() {
    this.setState({
      costas: listOfCostas
    });
  }

  render() {
    return (
      <Carousel space={1} items={{
        mobile: 2,
        landscape: 3,
        tablet: 4,
        desktop: 5

      }}>
        {this.state.costas.map((costa, index) => (
          <Costa key={index} {...costa} />
        ))}
      </Carousel>
    );
  }
}
