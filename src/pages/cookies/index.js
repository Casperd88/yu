import React from 'react'
import { ContentSection, Title } from './../../components/Section'

export default () => (
  <ContentSection>
    <Title>Cookies Policy</Title>
<p>Cookies are small text files placed on your computer and are common throughout websites across the internet. When used properly they are used to inform and encourage best practice by online websites and businesses, and to deliver and record timely and relevant information to users. We use them:
</p>
<ul>
<li>To collect anonymous information that will help us
understand visitors’ browsing habits on our website, and allow us to improve the Yachtup service;
</li><li>To compile statistical reports on website activity, e.g. number of visitors and the pages they visit;
</li><li>To temporarily store any information which you may enter in tools, such as calculators or demonstrations on our website;
</li><li>To define advertising on and off our website to certain users based on their browsing habits.
</li><li>We do not store any personal information in our cookies that other web users could read and understand.
</li></ul>

<p>
  <strong>
    What cookies does Yachtup use?
  </strong><br />
  Yachtup uses three types of cookie:</p>

<p>
  <strong>Session Cookies</strong><br />
  We use session cookies for the following purposes:
</p>

<p>
  To allow users of Yachtup to carry information across pages of our site – in particular to avoid having to re-submit security information or preferences. For example, we use session cookies to keep members logged in as they move from page to page. Our session cookies also ensure better security by logging them out of their account after a period of inactivity.
</p>
<p><strong>Persistent Cookies</strong><br />
We use persistent cookies for the following purposes:</p>
<ul><li>
To help us recognise you as a unique visitor when you return to our website and to allow us to tailor content or advertisements to match your preferred interests or to avoid showing you the same adverts repeatedly.
</li><li>To compile anonymous, aggregated statistics that allow us to understand how people use our site and to help us improve the structure of our website.
</li><li>To internally identify you by account ID, Name, Email address and location (geographic and computer ID/IP address)
</li></ul>

<p>
  <strong>Third Party Cookies</strong><br />
  We have relationships with third parties that serve cookies via our site. These are used for the following purposes:
</p>
<ul>
<li>To serve advertisements on third party sites and ensure they are timely and relevant.
</li><li>To control how often you are shown a particular advertisement.
</li><li>To tailor content to your preferences.
</li><li>To count and track the movement of anonymous users across our site(s)
</li><li>For website usage analysis and site
</li></ul>

<p><strong>Accepting or rejecting cookies</strong><br />
You can configure your internet browser to warn you each time a new cookie is about to be stored on your computer so that you may make a decision whether to accept or reject it.
</p>
<p>To accept or refuse cookies on your internet browser, please refer to its guide:
</p>

<ul>
  <li>Microsoft Internet Explorer</li>
  <li>Firefox</li>
  <li>Google Chrome</li>
  <li>Apple Safari</li>
</ul>

<p>Please note that some parts of our website may not function properly if you reject cookies.</p>
<p>
<strong>Further information about cookies</strong><br />
The Interactive Advertising Bureau is an industry body that has produced a series of web pages which explain how cookies work and how they can be managed.
</p>
<p>
  <a href="http://www.iabuk.net/policy/briefings/iab-fact-sheet-may-2012-internet-cookies">http://www.iabuk.net/policy/briefings/iab-fact-sheet-may-2012-internet-cookies</a>
</p>
  </ContentSection>
)
