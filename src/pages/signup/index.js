import React from 'react';
import SignupForm from './../../components/SignupForm';
import Notifications from './../../components/Notifications';
import { FormSection, Title } from './../../components/Section';

export const SignUpComponent = () => (
  <FormSection>
    <Title>Join Yachtup</Title>
    <Notifications />
    <SignupForm />
  </FormSection>
);

export default SignUpComponent;
