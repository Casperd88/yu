import React from 'react';
import styled from 'styled-components';
import Button from './../../components/Button';
import Icon from './../../components/Icon';

const Wrapper = styled.section`
  display: flex;
  margin: 4em;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  @media (max-width: 600px) {
    flex-direction: column-reverse;
    margin: 1em;
  }
`

const Title = styled.h2`
  padding: 0.7em;
  margin: 0;
  font-size: 1.4em;
  width: 100%;
`

const Plan = styled.div`
  display: flex;
  flex-direction: column;
  width: 300px;
  background-color: ${({primary, theme: {colors}}) => primary ? colors.primary : '#fff'};
  background-image: url('/images/pattern.png');
  color: ${({primary, theme: {colors}}) => primary ? '#fff' : colors.text};
  box-shadow: ${({primary}) => primary ? '0 0 2em rgba(75,99,97,.3)' : 'none'};
  margin: 1em 0;
  ${Title} {
    color: ${({primary, theme: {colors}}) => primary ? '#fff' : colors.bold};
    background-color: ${({primary, theme: {colors}}) => primary ? 'rgba(0,0,0,.2)' : '#fff'};
    border-bottom: ${({primary, theme: {colors}}) => primary ? 'none' : `1px ${colors.muted} solid`};
  }
`

const CallToAction = styled.div`
  display: inline-block;
  margin: 1em;
`

const PriceWrapper = styled.span`
  vertical-align: baseline;
  display: block;
  text-align: center;
`
const Price = styled.span`
  font-size: 2.4em;
  color: ${({theme: {colors}, color = 'bold'}) => colors[color]};
`

const CheckList = styled.ul`
  margin: 1.5em;
  padding: 0;
  display: flex;
  flex-grow: 1;
  flex-direction: column;
`;

const Item = styled.li`
  display: flex;
  align-items: center;
  padding: 1em 0;
  svg {
    margin-right: 1em;
  }
`

const Annotations = styled.p`
  margin: 1.5em 1.5em 3em 1.5em;
  padding: 0;
  text-align: center;
  color: ${({theme: {colors}}) => colors.text};
  font-size: 0.8em;
  line-height: 2em;
`
const Container = styled.div`
  display: block;
`

export default () => (
  <Container>
    <Wrapper>

      <Plan>
        <Title>Standard</Title>
        <CheckList>
          <Item>
            <Icon name={'check'} /> Unlimited Photos
          </Item>
          <Item>
            <Icon name={'check'} /> Yachtup Guarantee*
          </Item>
          <Item>
            <Icon name={'check'} /> Cancel Anytime
          </Item>
        </CheckList>
        <CallToAction>
          <PriceWrapper>
            <Price>€15</Price><br />per week
          </PriceWrapper>
        </CallToAction>
        <CallToAction>
          <Button color={'text'} outline={true} fullwidth={true}>
            Choose plan
          </Button>
        </CallToAction>

      </Plan>

      <Plan primary={true}>

        <Title>Yachtup&#x2122; Broker Service</Title>
        <CheckList>
          <Item>
            <Icon name={'check'} /> No Cure No Pay
          </Item>
          <Item>
            <Icon name={'check'} /> Certified Broker
          </Item>
          <Item>
            <Icon name={'check'} /> Competetive Commision
          </Item>
        </CheckList>
        <CallToAction>
          <PriceWrapper>
            <Price color={'white'}>4.5%</Price><br /> commission fee**
          </PriceWrapper>
        </CallToAction>
        <CallToAction>
          <Button color={'white'} outline={true} fullwidth={true}>
            Choose plan
          </Button>
        </CallToAction>

      </Plan>
    </Wrapper>
    <Annotations>
      (*) If your listing doesn't sell within 6 months we will keep it up
      for free untill a successfull sale is reached.<br />
      (**) Commission fee is paid after a successfull sale by a Certified Yachtup Broker of choice. <a href="/">Learn more</a>
    </Annotations>
  </Container>
);
