import React from 'react';
import ResetPasswordForm from './../../components/ResetPasswordForm';
import {connect} from 'react-redux';
import { FormSection, Title } from './../../components/Section';
import {resetPassword} from './../../actions/auth';

export const LoginComponent = ({resetPassword}) => (
  <FormSection>
    <Title>Reset password</Title>
    <ResetPasswordForm resetPassword={resetPassword} />
  </FormSection>
);

export default connect(null, (dispatch) => ({
  resetPassword: (email) => dispatch(resetPassword(email))
}))(LoginComponent);
