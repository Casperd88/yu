import React, { Component } from 'react';
import Aux from 'react-aux';
import { Section, Title as SectionTitle } from './../../components/Section';
import SalesPanel from './../../components/SalesPanel';
import RecentListings from './../../containers/RecentListings';
import SearchForm from './../../components/SearchForm';
import Costas from './../../containers/Costas';
import styled from 'styled-components';

const Homepage = styled.section`
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  box-sizing: border-box;
  padding: 6em 4em 4em 4em;
  margin-top: -4.8em;
  @media (max-width: 600px) {
    padding: 4em 2em 2em 2em;
  }
  background-image: url('/images/headers/stock4.jpg');
  background-size: cover;
  background-position: center;
  box-shadow: inset 0 0 0 10000px rgba(26, 45, 41, 0.65);
`;

const HeadLine = styled.h1`
  font-size: 2em;
  text-transform: uppercase;
  color: #fff;
  margin: 1.2em 0;
  padding: 0;
  font-weight: 500;
  letter-spacing: 0.2em;
  text-align: center;
  line-height: 1.2em;
  @media (max-width: 800px) {
    font-size: 1.6em;
  }
`

const TagLine = styled.h3`
  font-size: 1em;
  padding: 0.50em 1.4em 0.35em 1.4em;
  margin: 1.2em 0;
  background: rgba(44, 53, 53, 0.95);
  color: #fff;
  text-align: center;
  border-radius: 0.3em;
`;

/**/

class Home extends Component {

  render() {
    return (
      <Aux>
        <Homepage>
          <HeadLine>
            Find Your Dream Yacht in Sunny Spain
          </HeadLine>
          <SearchForm />
          <TagLine>
            Shop over {(10000).toLocaleString()} sail &amp; motor yachts along the Spanish costas
          </TagLine>
        </Homepage>

        <Section>
          <SectionTitle>Recent Listings</SectionTitle>
          <RecentListings />
        </Section>

        <Section>
          <SectionTitle>Browse by Costa</SectionTitle>
          <Costas />
        </Section>

        <SalesPanel />
      </Aux>
    );
  }
}

export default Home;
