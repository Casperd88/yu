import React from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import Button from '../../components/Button'
import { notify } from './../../actions/notification'

const Canvas = styled.div`
  height: 500px;
  width: 500px;
  background: #fff;
  margin: 4em;
`

const Hello = ({notify}) => (
  <Canvas>

    <p><Button onClick={() => {
      notify(
        'A network error ocured',
        false,
        'error')
    }}>Show Error</Button></p>

    <p><Button onClick={() => {
      notify(
        'A network error ocured',
        'More details about this error are mentioned here in the body section of the notification.',
        'error'
      )
    }}>Show Extended Error</Button></p>

    <p><Button onClick={() => {
      notify(
        'Consider this a warning',
        false,
        'warning')
    }}>Show Warning</Button></p>

    <p><Button onClick={() => {
      notify(
        'Consider this a warning',
        'More details about this warning are mentioned here in the body section of the notification.',
        'warning'
      )
    }}>Show Extended Warning</Button></p>

    <p><Button onClick={() => {
      notify(
        'Consider this a message',
        false,
        'info')
    }}>Show Info</Button></p>

    <p><Button onClick={() => {
      notify(
        'Consider this a message',
        'More details about this message are mentioned here in the body section of the notification.',
        'info'
      )
    }}>Show Extended Info</Button></p>

    <p><Button onClick={() => {
      notify(
        'Consider this a victory',
        false,
        'success')
    }}>Show Success</Button></p>

    <p><Button onClick={() => {
      notify(
        'Consider this a victory',
        'More details about this victory are mentioned here in the body section of the notification.',
        'success'
      )
    }}>Show Extended Success</Button></p>

  </Canvas>
)

const mapToDispatch = (dispatch) => ({
  notify: (title, message, type) => {
    dispatch(notify(title, message, type))
  }
})

export default connect(null, mapToDispatch)(Hello)
