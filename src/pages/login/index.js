import React from 'react'
import LoginForm from './../../components/LoginForm'
import { FormSection, Title } from './../../components/Section'
import {connect} from 'react-redux'
import {authLogin} from './../../actions/auth'
import {notify} from './../../actions/notification'

export const LoginComponent = ({login, notify}) => (
  <FormSection>
    <Title>Log in</Title>
    <LoginForm loginHandler={login} notify={notify}/>
  </FormSection>
)

export default connect(null, (dispatch) => ({
  login: (username, password, persist) => {
    return dispatch(authLogin(username, password, persist))
  },
  notify: (...args) => dispatch(notify(...args))
}))(LoginComponent)
