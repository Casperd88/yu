import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { lighten, darken } from 'polished';
import AspectBox from './../AspectBox';
import Icon from './../Icon';

const PhotoCardLabel = styled.h2`
  display: inline-block;
  font-size: 1.2em;
  margin: 0;
  padding: 0;
  color: white;
`;

const PhotoCardContent = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
`

const StyledLink = ({ className, children, to, ...props }) => (
  <Link className={className} to={to} {...props}>
    {children}
  </Link>
);

const PhotoCardAction = styled.span`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  width: 3em;
  height: 3em;
  background: ${({theme: {colors}}) => colors.primary};
  color: white;
  position: absolute;
  bottom: 1em;
  border-radius: 3em;
  opacity: 0;
  transform: translate3d(0, 0, 0);
  transition: all 200ms ease-in-out;
  &:hover {
    background: ${({theme: {colors}}) => lighten(0.1, colors.primary)};
  }
  &:focus, &:active {
    background: ${({theme: {colors}}) => darken(0.1, colors.primary)};
  }
`;

const PhotoCardWrapper = styled(StyledLink)`
  width: 100%;
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  display: block;
  box-shadow: inset 0 0 0 10000px rgba(26, 45, 41, 0.65);
  &:hover ${PhotoCardAction} {
    opacity: 1;
    transform: translate3d(0, -1em, 0);
  }
`;

const PhotoCard = ({label, image, link}) => {
  return (
    <PhotoCardWrapper to={link} style={{
      backgroundImage: `url('${image}')`
    }}>
      <AspectBox ratio={210/148}>
        <PhotoCardContent>
          <PhotoCardLabel>
            {label}
          </PhotoCardLabel>
          <PhotoCardAction>
            <Icon name="search" />
          </PhotoCardAction>
        </PhotoCardContent>
      </AspectBox>
    </PhotoCardWrapper>
  )
};

export default PhotoCard;
