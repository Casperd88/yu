import React, { Component } from 'react';
import { Formik, Field } from 'formik';
import Yup from 'yup';
import YInput from './../YInput';
import { Link } from 'react-router-dom';
import firebase from 'firebase';
import {withRouter} from "react-router-dom";
import Button from './../Button';
import { Hr, Spacer, TextCenter, SmallText } from './../Atoms';
require("firebase/firestore");

/*<div className="field">
  <button
    className={classNames(
      'button',
      'is-medium',
      'is-primary',
      'is-fullwidth',
      'is-outlined',
      {
        'is-loading': isSubmitting
      }
    )}
    type="submit"
    disabled={isSubmitting}
  >
    Sign up
  </button>
</div>*/

const formSchema = Yup.object().shape({
  firstName: Yup.string().required('First name is required'),
  lastName: Yup.string().required('Last name is required'),
  email: Yup.string().email('Invalid email address').required('Email is required'),
  password: Yup.string().required('Password is required')
});

async function createUser(email, password, profile) {

  const user = await firebase
    .auth()
      .createUserWithEmailAndPassword(email, password);

  await firebase
    .firestore()
      .collection('users')
        .doc(user.uid)
          .set(profile);

  await user.sendEmailVerification();

  return user;
}

class SignupForm extends Component {

  initialValues = {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    remember: false,
  }

  async submitHandler({firstName, lastName, email, password},
  {
    props,
    setSubmitting,
    setErrors,
    setStatus,
  }) {
    try {
      await createUser(email, password, {firstName, lastName});
      this.props.history.push('/');
    } catch (error) {
      console.error('error', error);
    }
    setSubmitting(false);
  }

  renderForm({values, handleSubmit, isSubmitting}) {

    return (
      <form onSubmit={handleSubmit}>

        <Field
          type="text"
          name="firstName"
          placeholder="First name"
          component={YInput}
        />

        <Field
          type="text"
          name="lastName"
          placeholder="Last name"
          component={YInput}
        />

        <Field
          type="email"
          name="email"
          placeholder="Email"
          component={YInput}
          icons={{left: 'envelope'}}
        />

        <Field
          type="password"
          name="password"
          placeholder="Password"
          component={YInput}
          icons={{left: 'lock'}}
        />

        <Spacer>
          <Button fullwidth={true} outline={true}>Sign up</Button>
        </Spacer>

        <Spacer>
          <TextCenter>
            Already have an account? <Link to="/login">Log in</Link>
          </TextCenter>
        </Spacer>

        <Hr/>

        <TextCenter>
          <SmallText>
            By signing up you agree to our <a href="/">terms of use</a> and <a href="/">privacy policy</a>.
          </SmallText>
        </TextCenter>

      </form>
    );
  }

  render() {
    return (
      <Formik
        initialValues={this.initialValues}
        validationSchema={formSchema}
        onSubmit={this.submitHandler.bind(this)}
        render={this.renderForm.bind(this)}
      />
    )
  }
}

export default withRouter(SignupForm);
