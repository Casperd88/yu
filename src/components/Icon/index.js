import React from 'react';

const Icon = ({name, type = 's', ...props}) => (
  <i {...props} className={`fa${type} fa-${name}`}></i>
);

export const BrandIcon = ({name}) => (<i className={`fab fa-${name}`}></i>);

export default Icon;
