import React, { Component } from 'react';
import styled from 'styled-components';
import { Formik, Field } from 'formik';
import Yup from 'yup';
import YInput from './../YInput';
import Button from './../Button';
import { Link, withRouter } from 'react-router-dom';
import { Hr, Spacer, TextCenter } from './../Atoms';

const loginSchema = Yup.object().shape({
  email: Yup.string().email('Invalid email address').required('Email is required'),
  password: Yup.string().required('Password is required')
});

const CheckboxField = styled.label`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  vertical-align: middle;
  cursor: pointer;
  > span {
    position: relative;
    top: 3px;
  }
`

class LoginForm extends Component {

  state = {
    email: '',
    password: '',
    persist: true
  }

  async submitHandler({email, password, persist},
  {
    props,
    setSubmitting,
    setErrors,
    setStatus,
  }) {
    try {
      await this.props.loginHandler(email, password, persist)
      this.props.history.push('/')
    } catch (error) {
      this.props.notify(error.code, error.message)
    }
    setSubmitting(false)
  }

  renderForm({values, handleSubmit, isSubmitting, setFieldValue}) {

    return (
      <form onSubmit={handleSubmit}>

        <Field
          type="email"
          name="email"
          placeholder="Email"
          component={YInput}
          icons={{left: 'envelope'}}
        />

        <Field
          type="password"
          name="password"
          placeholder="Password"
          component={YInput}
          icons={{left: 'lock'}}
        />

        <Spacer>
          <CheckboxField>
            <Field
              type="checkbox"
              name="persist"
              checked={this.state.persist}
              onChange={(event) => {
                this.setState({persist: event.target.checked})
                setFieldValue('persist', event.target.checked)
              }}
              component={YInput}
            />
            <span>Remember me</span>
          </CheckboxField>
        </Spacer>

        <Spacer>
          <Button loading={isSubmitting} fullwidth={true} outline={true} type="submit">Log in</Button>
        </Spacer>
        <Spacer>
          <TextCenter>
            <Link to="/recover-password">Forgot password?</Link>
          </TextCenter>
        </Spacer>
        <Hr />
        <TextCenter>
          Don't have an account? <Link to="/signup">Sign up</Link>
        </TextCenter>
      </form>
    );
  }

  render() {
    return (
      <Formik
        initialValues={this.state}
        validationSchema={loginSchema}
        onSubmit={this.submitHandler.bind(this)}
        render={this.renderForm.bind(this)}
      />
    )
  }
}

export default withRouter(LoginForm);
