import React from 'react';
import styled from 'styled-components';
import {LinkButton} from '../Button';
import { LogoSvg } from './../Logo';

const Wrapper = styled.section`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 4em;
  background-color: ${({theme: {colors}}) => colors.primary};
  background-position: -3em 0;
  background-image: url('/images/pattern.png');
  background-repeat: repeat;
  background-size: auto;
  color: #fff;
  @media (max-width: 800px) {
    padding: 1em;
    background-size: auto;
  }
`;

const CallToAction = styled.div`
  text-align: center;
  border-radius: 0.3em;
  padding: 1em;
  width: 100%;
  max-width: 460px;
  @media (max-width: 400px) {
    padding: 0;
  }

`;

const FreeBeer = styled.div`
  margin-top: 1em;
  display: inline-block;
  background: rgba(0,0,0,0.3);
  height: 2.4em;
  line-height: 2.6em;
  border-radius: 2.4em;
  padding: 0 2em;
`;

const WavePattern = styled.div`
  padding-top: 19px;
  background: url('/images/wave.png') repeat-x 0 0;
`

const SalesPanel = () => {
  return (
    <WavePattern>
      <Wrapper>
        <CallToAction>
          <LinkButton to="/list" fullwidth={true} color={'secondary'} Icon={LogoSvg}>List my boat for sale</LinkButton>
          <FreeBeer>
            It's totally free!
          </FreeBeer>
        </CallToAction>
      </Wrapper>
    </WavePattern>
  )
}

export default SalesPanel;
