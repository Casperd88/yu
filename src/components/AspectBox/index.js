import React from 'react';
import styled from 'styled-components';

const AspectBoxOuter = styled.div`
  width: 100%;
  position: relative;
  padding-top: calc(100% * ${({ratio}) => ratio});
  overflow: hidden;
`;

const AspectBoxInner = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
`;

const AspectBox = ({children, ...props}) => (
  <AspectBoxOuter {...props}>
    <AspectBoxInner>
      {children}
    </AspectBoxInner>
  </AspectBoxOuter>
);

export default AspectBox;
