import React, { Component } from 'react';
import { Formik, Field } from 'formik';
import Yup from 'yup';
import YInput from './../YInput';
import Button from './../Button';
import { Hr, Spacer, TextCenter } from './../Atoms';
import { Link } from 'react-router-dom';

const loginSchema = Yup.object().shape({
  email: Yup.string().email('Invalid email address').required('Email is required')
});

class LoginForm extends Component {

  initialValues = {
    email: ''
  }

  submitHandler({email, password, persist},
  {
    props,
    setSubmitting,
    setErrors,
    setStatus,
  }) {
    this.props.resetPassword(email)
      .then(() => setSubmitting(false));
  }

  renderForm({values, handleSubmit, isSubmitting}) {

    return (
      <form onSubmit={handleSubmit}>

        <Spacer>
          Enter the email address associated with your account, and we’ll email you a link to reset your password.
        </Spacer>

        <Spacer>
          <Field
            type="email"
            name="email"
            placeholder="Email"
            component={YInput}
            icons={{left: 'envelope'}}
          />
        </Spacer>

        <Spacer>
          <Button fullwidth={true} outline={true} disabled={isSubmitting} loading={isSubmitting} type="submit">
            Send reset link
          </Button>
        </Spacer>

        <Hr />
        <TextCenter>
          <Link to="/login">Back to login</Link>
        </TextCenter>

      </form>
    );
  }

  render() {
    return (
      <Formik
        initialValues={this.initialValues}
        validationSchema={loginSchema}
        onSubmit={this.submitHandler.bind(this)}
        render={this.renderForm.bind(this)}
      />
    )
  }
}

export default LoginForm;
