import React from 'react';
import Love from './../Love';
import Carousel from './../Carousel';
import AspectBox from './../AspectBox';
import styled from 'styled-components';

const LoveListing = styled(Love)`
  position: absolute;
  right: 0.8em;
  top: 0.8em;
  z-index: 100;
`;

const Wrapper = styled.div`
  background: white;
  box-shadow: ${({theme}) => theme.boxShadow};
  position: relative;
  overflow: hidden;
`;

const Title = styled.span`
  display: block;
  color: white;
  height: 2.8em;
  width: 100%;
  background: ${({theme: {colors}}) => colors.primary};
  line-height: 3em;
  padding: 0 1.4em;
`;

const Visual = styled.div`
  position: relative;
  line-height: 0;
`;

const Photo = styled.div`
  display: inline-block;
  position: relative;
`;

const PhotoAspectBox = styled(AspectBox)`
  img {
    width: 100%;
    height: 100%;
  }
`;

const Broker = styled.a`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  right: 1.6em;
  bottom: -2em;
  overflow: hidden;
  height: 4em;
  width: 4em;
  background: #ecf3f2;
  border-radius: 4em;
  border: 2px #fff solid;
  box-shadow: 0 1px 3px rgba(0,0,0,0.3);
`;

const Details = styled.div`
  padding: 1.2em;
  line-height: 1.8em;
`;

const Price = styled.span`
  display: block;
  color: ${({theme: {colors}}) => colors.bold};
`;

const Specs = styled.span`
  display: block;
  font-size: 0.85em;
  color: #677371;
  line-height: 1.6em;
`;

const ListCard = ({
  photos,
  title,
  price,
  year,
  location,
  length,
  brokerName,
  brokerLogo
}) => {
  return (
    <Wrapper>
      <LoveListing />
      <Visual>
        <PhotoAspectBox ratio={3/4}>
        <Photo>
          <Carousel photos={true} items={{mobile: 1, landscape: 1, tablet: 1, desktop: 1}}>
          {photos.map((photo, index) => (
            <img key={index} src={photo} alt={title} title={title} />
          ))}
          </Carousel>
        </Photo>
      </PhotoAspectBox>
        <Title>{title}</Title>

        {brokerLogo && (
          <Broker>
            <img src={brokerLogo} alt={brokerName} title={brokerName} />
          </Broker>
        )}
      </Visual>
      <Details>
        <Price>
          &euro; {price.toLocaleString()}
        </Price>
        <Specs>
          {location} &middot; {year} &middot; {length}ft
        </Specs>
      </Details>
    </Wrapper>
  )
};

export default ListCard;
