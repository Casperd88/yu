import React from 'react'
import { connect } from 'react-redux'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import styled, { injectGlobal } from 'styled-components'
import Icon from './../Icon'
import { removeNotification } from './../../actions/notification'

injectGlobal`
  @keyframes notificationprogress {
    0% {
      width: 100%;
    }
    100% {
      width: 0%;
    }
  }
`

const Wrapper = styled.div`
  display: block;
  padding: 1.2em;
  position: relative;
  border-radius: 0.2em;
  background: ${({theme: {colors}, type}) => colors[type]};
  color: #fff;
  overflow: hidden;
  margin-bottom: 1em;
  box-shadow: ${({theme}) => theme.boxShadowLarge};
  &:before {
    display: block;
    content: '';
    position: absolute;
    height: 4px;
    width: 100%;
    right: 0;
    bottom: 0;
    background: rgba(255,255,255,0.5);
    animation-fill-mode: both;
    animation: notificationprogress 10s normal ease-out;
  }
`

const Label = styled.span`
  display: inline-block;
  line-height: 1.25em;
  height: 1em;
`

const IconWrapper = styled.span`
  display: inline-flex;
  opacity: 0.5;
  line-height: 0;
`
const Header = styled.h2`
  margin: 0;
  display: flex;
  padding: 0;
  font-size: 1em;
  font-weight: normal;
  align-items: center;
  justify-content: space-between;
  ${Label} {
    margin-left: 0.75em;
  }
`

const Container = styled.div`
  position: fixed;
  bottom: 1em;
  left: calc(1em);
  width: calc(100vw - 2em);
  z-index: 2000;
  @media (min-width: 800px) {
    left: calc(50% - 200px);
    width: 400px;
    bottom: auto;
    top: 2em;
  }
`

const Body = styled.p`
  margin: 1.2em 0 0 0;
`
const Title = styled.span`
  display: inline-flex;
  align-items: center;
  justify-content: center;
`

const Close = styled.a`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  height: 1em;
  border-left: 1px rgba(255, 255, 255, 0.5) solid;
  color: rgba(255, 255, 255, 0.5);
  font-size: 1em;
  cursor: pointer;
  transition: all 200ms ease-in;
  &:hover {
    color: #fff;
  }
  svg {
    margin-left: 0.5em;
  }
`

const Notification = ({title, message, type, onClose}) => {

  const typeIconMap = {
    info: 'info-circle',
    error: 'exclamation-circle',
    warning: 'exclamation-triangle',
    success: 'check-circle'
  }

  return (
    <Wrapper type={type}>
      <Header>
        <Title>
          <IconWrapper>
            <Icon name={typeIconMap[type]} />
          </IconWrapper>
          <Label>{title}</Label>
        </Title>
        <Close onClick={onClose}>
          <Icon name={'times'} />
        </Close>
      </Header>
      {message && (
        <Body>{message}</Body>
      )}
    </Wrapper>
  )
}

injectGlobal`
  .fade-enter {
    opacity: 0;
    transform: scale(0.9);
  }

  .fade-enter.fade-enter-active {
    opacity: 1;
    transform: scale(1);
    transition: all 200ms ease-in;
  }

  .fade-exit {
    transition: all 200ms ease-in;
    opacity: 1;
    transform: scale(1);
  }

  .fade-exit.fade-exit-active {
    opacity: 0;
    transform: scale(0.9);
  }
`

const Notifications = ({notifications, close}) => (
  <Container>
    <TransitionGroup>
      {notifications.map(({title, message, type, $id}) => (
        <CSSTransition key={$id} classNames="fade" timeout={{ enter: 200, exit: 200 }}>
          <Notification
            title={title}
            message={message}
            type={type}
            onClose={() => close($id)}/>
        </CSSTransition>
      ))}
    </TransitionGroup>
  </Container>
)

const mapState = ({notifications}) => ({notifications});

const mapDispatch = (dispatch) => ({
  close: ($id) => dispatch(removeNotification($id))
})
export default connect(mapState, mapDispatch)(Notifications);
