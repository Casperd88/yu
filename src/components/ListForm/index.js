import React, { Component } from 'react';
import { Formik, Field } from 'formik';
import Yup from 'yup';
import YInput from './../YInput';
import classNames from 'classnames';
import { Link } from 'react-router-dom';

const FormContainer = ({children, hideprev, handleSubmit, isSubmitting}) => (
  <form onSubmit={handleSubmit}>
    {children}
    <div className="field">

      <div className="columns">
        <div className="column">
          {!hideprev && (<button
            className={classNames(
              'button',
              'is-medium',
              'is-primary',
              'is-outlined',
              'is-fullwidth',
              {
                'is-loading': isSubmitting
              }
            )}
            type="button"
          >
            Previous
          </button>)}
        </div>
        <div className="column">

          <button
            className={classNames(
              'button',
              'is-medium',
              'is-primary',
              'is-fullwidth',
              {
                'is-loading': isSubmitting
              }
            )}
            type="submit"
            disabled={isSubmitting}
          >
            Next
          </button>

        </div>
      </div>
    </div>
    <hr />
    <p className="has-text-centered">
      <Link to="/signup">Skip</Link>
    </p>
  </form>
);

const Steps = [

  // Step 1
  {
    initialValues: {
      email: ''
    },
    validationSchema: Yup.object().shape({
      email: Yup.string()
        .email('Invalid email address')
          .required('Email is required'),
    }),
    render: ({values, handleSubmit, isSubmitting}) => (
      <FormContainer hideprev={true} handleSubmit={handleSubmit} isSubmitting={isSubmitting}>
        <Field
          type="text"
          name="manufacturer"
          placeholder="Manufacturer"
          component={YInput}
          icons={{left: 'ship'}}
        />

        <Field
          type="text"
          name="model"
          placeholder="Make / Model"
          component={YInput}
        />
      </FormContainer>
    ),
    onSubmit: () => console.log('step 1 submitted')
  },

  // Step 2
  {
    initialValues: {
      password: ''
    },
    validationSchema: Yup.object().shape({
      password: Yup.string()
          .required('Password is required'),
    }),
    render: ({values, handleSubmit, isSubmitting}) => (
      <FormContainer handleSubmit={handleSubmit} isSubmitting={isSubmitting}>
        <Field
          type="password"
          name="password"
          placeholder="Password"
          component={YInput}
          icons={{left: 'lock'}}
        />
      </FormContainer>
    ),
    onSubmit: () => console.log('step 2 submitted')
  },
];

class StepForm extends Component {

  componentWillMount() {
    this.setState({step: 0});
  }

  renderForm({
    initialValues,
    validationSchema,
    onSubmit,
    render
  }, index) {
    return index === this.state.step ? (
      <Formik
        key={index}
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={async (values, {setSubmitting}) => {
          await onSubmit();
          setSubmitting(false);
          this.setState({step: this.state.step + 1});
        }}
        render={render}
      />
    ) : null;
  }

  render() {
    return (
      <div>
        {this.props.steps.map(this.renderForm.bind(this))}
      </div>
    )
  }
}

export default () => (
  <StepForm steps={Steps}></StepForm>
);
