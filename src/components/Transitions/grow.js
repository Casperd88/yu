import React from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group'

const duration = 300;

const defaultStyle = {
  transition: `all ${duration}ms ease-in-out`,
  opacity: 0,
  transform: 'scale(0.5)'
}

const transitionStyles = {
  entering: { opacity: 0, transform: 'scale(0.5)' },
  entered: { opacity: 1, transform: 'scale(1)' },
};

export const Fade = ({ in: inProp }) => (
  <CSSTransition in={inProp} timeout={duration}>
    {(state) => (
      <div style={{
        ...defaultStyle,
        ...transitionStyles[state]
      }}>
        I'm A fade Transition!
      </div>
    )}
  </CSSTransition>
);

export const Fader = ({children}) => (
  <TransitionGroup>
    {children.map((child, i) => (
      <Fade key={child.key}>
        {child}
      </Fade>
    ))}
  </TransitionGroup>
)
