import React from 'react'
import styled from 'styled-components'
import FaIcon from './../Icon'
import DropDownMenu from './../DropDownMenu'

const OuterWrapper = styled.span`
  display: inline-flex;
  justify-content: center;
  align-items: center;
`
const Badge = styled.span`
  display: inline-block;
  text-align: center;
  position: absolute;
  right: -6px;
  top: -6px;
  width: 18px;
  height: 18px;
  line-height: 20px;
  font-size: 12px;
  color: white;
  background: ${({theme: {colors}}) => colors.notification};
  border-radius: 10px;
  font-weight: bold;
`

const Arrow = styled.span`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  width: 12px;
  height: 12px;
  margin: 0 0 0 0.5em;
  transform: rotate(0);
  transition: all 300ms ease-in-out;
`

const IconWrapper = styled.span`
  display: inline-flex;
  font-size: 0;
  justify-content: center;
  align-items: center;
  position: relative;
`

const Wrapper = styled.a`
  display: inline-flex;
  padding: 0 5px;
  height: 40px;
  margin-left: 5px;
  margin-top: 5px;
  align-items: center;
  border-radius: 0.3em;
  color: ${({isLight, open, theme: {colors}}) => open ? 'rgba(255,255,255,0.5)' : !isLight ? 'rgba(255,255,255,0.3)' : colors.muted};
  transition: all 200ms ease;
  position: relative;
  background: ${({theme, open, isLight}) => !open ? 'transparent' : isLight ? theme.colors.muted : 'rgba(44,53,53,0.5)'};
  ${Arrow} {
    opacity: ${({open}) => open ? 1 : 0};
    position: relative;
    transform: rotate(${({open}) => open ? '180deg' : 0});
  }
  ${IconWrapper} {
    svg {
      width: 32px;
      height: 32px;
      fill: currentColor;
    }
  }
  &:hover {
    color: ${({isLight, open, theme: {colors}}) => open ? 'rgba(255,255,255,0.5)' : !isLight ? 'rgba(255,255,255,0.3)' : colors.muted};

    ${Arrow} {
      opacity: 1;
      color: ${({open}) => open ? '#fff' : 'inherit'};
    }
  }
  ${Badge} {
    opacity: ${({open}) => open ? 0 : 1};
    transform: scale(${({open}) => open ? 0.5 : 1}) translate3d(0,${({open}) => open ? '5px' : 0},0);
    transition: all 300ms ease-in-out;
  }
  ${IconWrapper} {
    color: ${({open}) => open ? 'rgba(255,255,255,0.6)' : 'inherit'};
  }
  &:active, &:focus {
    color: rgba(255,255,255,0.5);
    background: ${({theme, isLight}) => isLight ? theme.colors.darkgrey : 'rgba(44,53,53,0.5)'};
    ${IconWrapper} {
      color: #fff;
    }
    ${Badge} {
      opacity: 0;
      transform: scale(0.5);
    }
  }
`

const JewelButton = ({isLight, open, Icon, count = 0, ...props}) => (
  <Wrapper isLight={isLight} open={open} {...props}>
    <IconWrapper>
      {count > 0 && (<Badge>{count}</Badge>)}
      <Icon />
    </IconWrapper>
    <Arrow>
      <FaIcon name={'angle-down'} />
    </Arrow>
  </Wrapper>
)

export default ({children, ...props}) => (
  <DropDownMenu ToggleButton={({open, toggle}) => (
    <OuterWrapper>
      <JewelButton onClick={toggle} open={open} {...props} />
    </OuterWrapper>
  )}>
    {children}
  </DropDownMenu>
)
