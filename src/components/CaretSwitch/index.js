import React from 'react'
import styled from 'styled-components'
import Icon from '../../components/Icon'

const Caret = styled.span`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  transform: rotate(0);
  transition: all 200ms ease-in-out;
  margin-left: 0.5em;
`
const Wrapper = styled.span`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  ${Caret} {
    transform: rotate(${({open}) => open ? '180deg' : 0});
  }
`

const Spacer = styled.span`
  display: inline-block;
  height: 1em;
  width: 1px;
  background-color: white;
`

const CaretSwitch = ({Component, spacer = false, open, transparent}) => (
  <Wrapper open={open} transparent={transparent}>
    <Component />
    {spacer && (
      <Spacer />
    )}
    <Caret>
      <Icon name={'angle-down'} />
    </Caret>
  </Wrapper>
)

export default CaretSwitch;
