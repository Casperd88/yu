import React from 'react'
import styled from 'styled-components'
import Icon from './../Icon'

const Divider = styled.span`
  display: inline-block;
  height: 1.5em;
  width: 1px;
  background: ${({theme}) => theme.colors.muted};
  margin-left: 0.5em;

`

const Arrow = styled.span`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  width: 12px;
  height: 12px;
  margin: 0 0 0 0.5em;
  transform: rotate(0);
  transition: transform 300ms ease-in-out;
`
const LocaleButton = styled.a`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  z-index: 300;
  user-select: none;
  position: fixed;
  right: 1em;
  bottom: 1em;
  @media (min-width: 800px) {
    right: 2em;
    bottom: 2em;
  }
  padding: 0.5em 0.5em;
  border-radius: 0.3em;
  border: 1px rgba(0,0,0,0.2) solid;
  box-shadow: ${({theme}) => theme.boxShadowLarge};
  background-color: ${({theme}) => theme.colors.white};
  color: ${({theme}) => theme.colors.text};
  transition: all 100ms ease-in-out;
  &:hover {
    color: ${({theme}) => theme.colors.bold};
  }
  svg {
    margin: 0 0.5em;
    color: ${({theme}) => theme.colors.muted};
  }
  &:active, &:focus {
    background: ${({theme}) => theme.colors.darkgrey};
    color: white;
    svg {
      color: rgba(255, 255, 255, 0.5);
    }
    ${Arrow} {
      transform: rotate(180deg);
    }
    ${Divider} {
      background: rgba(255, 255, 255, 0.5);
    }
  }
`
const Label = styled.span`
  position: relative;
  top: 0.1em;
  display: none;
  @media (min-width: 600px) {
    display: inline-block;
  }
`

const IconWrapper = styled.span`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  width: 1em;
  height: 1em;
  margin: 0 0.3em;
  svg {
    color: ${({theme}) => theme.colors.text};
    @media (min-width: 600px) {
      color: ${({theme}) => theme.colors.muted};
    }
  }
`


export default () => (
  <LocaleButton>
    <IconWrapper>
      <Icon name={'globe'} />
    </IconWrapper>
    <Label>Language &amp; Currency</Label>
    <Divider />
    <Arrow><Icon name={'angle-down'} /></Arrow>
  </LocaleButton>
)
