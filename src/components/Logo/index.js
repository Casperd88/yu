import React from 'react';
import styled from 'styled-components';

export const LogoSvg = (props) => (
  <svg {...props} viewBox="0 0 528.3 515.77">
    <path d="M305,.47a61.75,61.75,0,0,0-35.56,116.32L262.85,170l-54.75-6.77a21,21,0,1,0-5.16,41.72l54.75,6.77L225.94,468.57C151.2,450.08,97.4,384.38,93.23,308.25l14.22,18.23a21,21,0,0,0,33.14-25.85L95.46,242.77A21,21,0,0,0,66,239.12L8.1,284.28a21,21,0,1,0,25.85,33.14L51,304.1c2.62,105.69,82.47,196.67,190.48,210s207.63-55.46,235.9-157.34l13.33,17.08A21,21,0,1,0,523.85,348l-45.13-57.86a21,21,0,0,0-29.49-3.65l-57.87,45.15a21,21,0,1,0,25.85,33.14l18.2-14.2c-22.6,72.81-90.76,123.4-167.75,123.13l31.75-256.86,54.75,6.77a21,21,0,1,0,5.16-41.72l-54.75-6.77L311.14,122A61.75,61.75,0,0,0,305,.47Zm-5.16,41.74a19.71,19.71,0,1,1-22,17.15A19.73,19.73,0,0,1,299.83,42.21Z"/>
  </svg>
);

export const LogoWrapper = styled.span`
  height: 3.4em;
  width: 3.4em;
  border-radius: 3.4em;
  background: transparent;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  svg {
    width: 2.6em;
    height: 2.6em;
    fill: ${({theme: {colors}}) => colors.primary};
  }

`;

const Logo = () => (<LogoWrapper><LogoSvg /></LogoWrapper>);

export default Logo;
