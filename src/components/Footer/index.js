import styled from 'styled-components';
import React from 'react';
import { BrandIcon } from './../Icon';
import SelectBox from './../SelectBox';
import { Link } from 'react-router-dom';
import { Hr } from './../Atoms';

const Wrapper = styled.footer`
  background: #fff;
  padding: 1em 1.5em;
  @media (max-width: 800px) {
    margin: 0;
    padding: 0;
  }
`
const Copyright = styled.span`
  color: ${({theme: {colors}}) => colors.text};
  display: inline-flex;
  align-items: center;
  justify-content: flex-end;
  background: ${({theme: {colors}}) => colors.white};
  flex-direction: column;
  color: ${({theme: {colors}}) => colors.text};
  padding: 0 1.5em 1.5em 1.5em;
  font-size: 0.8em;
  line-height: 2em;
  text-align: center;
  svg {
    height: 3em;
    width: 3em;
    fill: currentColor;
    margin-bottom: 1.5em;
  }
`

const NavWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-end;
`
const Title = styled.h2`
margin: 0 0 0.5em 0;
padding: 0;
font-size: 1em;
color: ${({theme: {colors}}) => colors.bold};
`

const Nav = styled.nav`
  display: inline-flex;
  width: calc(100% / 4);
  flex-direction: column;
  padding: 1.5em 1.5em 0 1.5em;
  @media (max-width: 600px) {
    padding: 1.5em 1.5em 0 1.5em;
    width: 100%;
  }
  a {
    display: inline-block;
    margin: 0.2em 0;
    color: ${({theme: {colors}}) => colors.text};
    &:hover {
      color: ${({theme: {colors}}) => colors.primary};
    }
  }
`

const Social = styled.div`
  display: flex;
  flex-grow: 1;
  justify-content: flex-end;
  align-items: flex-start;
  flex-direction: row;
  @media (max-width: 600px) {
    flex-direction: row;
    justify-content: flex-start;
  }
`

const LegalLinks = styled.div`
  display: inline-block;
  text-align: right;
`

const SocialIcon = styled.span`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  height: 3em;
  width: 3em;
  border-radius: 0.3em;
  background: ${({theme: {colors}}) => colors.text};
  margin: 0.3em !important;
  color: #fff;
  > span {
    display: inline-block;
    font-size: 1.65em;
    line-height: 0;
  }
`;



export default () => (
  <Wrapper>
    <NavWrapper>

      <Nav>
        <SelectBox>
          <option>English</option>
          <option>Deutch</option>
          <option>Français</option>
          <option>Nederlands</option>
          <option>Español</option>

        </SelectBox>
        <SelectBox>
          <option>EUR</option>
          <option>USD</option>
          <option>GBP</option>
        </SelectBox>
      </Nav>

      <Nav>
        <Title>Learn</Title>
        <Link to="/">For Individuals</Link>
        <Link to="/">For Brokers</Link>
        <Link to="/">For Buyers</Link>
        <Link to="/">Valuation Calculator</Link>
        <Link to="/">Help Center</Link>
      </Nav>

      <Nav>
        <Title>Yachtup</Title>
        <Link to="/">Press</Link>
        <Link to="/">Team</Link>
        <Link to="/">Jobs</Link>
        <Link to="/">Blog</Link>
        <Link to="/">About</Link>
      </Nav>

      <Nav>
        <Social>
          <SocialIcon>
            <span>
              <BrandIcon name="facebook-f" />
            </span>
          </SocialIcon>
          <SocialIcon>
            <span>
              <BrandIcon name="instagram" />
            </span>
          </SocialIcon>
          <SocialIcon>
            <span>
              <BrandIcon name="twitter" />
            </span>
          </SocialIcon>
        </Social>
        <LegalLinks>
          <Link to="/">Terms</Link> &middot; <Link to="/">Privacy</Link>
        </LegalLinks>
      </Nav>

    </NavWrapper>

    <Hr />

    <Copyright>

      &copy; 2018 Yachtup. All rights reserved. Yachtup is a trading name for Seyco Holding Company Limited (company no. 10514941) and our offices are at Cov Business Resource Network, Whateleys Drive, Kenilworth, United Kingdom, CV8 2GY.<br />

    </Copyright>

  </Wrapper>
)
