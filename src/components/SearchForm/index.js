import React from 'react'
import styled from 'styled-components'
import { Formik, Field } from 'formik'
import YInput from './../YInput'
import Button from './../Button'
import Icon from './../Icon'

const Box = styled.div`
  display: inline-block;
`
const Flex = styled.div`
  display: flex;
  justify-content: ${({justify = 'flex-start'}) => justify};
`

const Wrapper = styled.div`
  background: rgba(16, 173, 158, 0.9);
  color: ${({theme: {colors}}) => colors.white};
  width: 100%;

  * {
    color: white !important;
    input {
      border-color: rgba(255,255,255,0.5) !important;
    }
    &::placeholder {
      color: white !important;
    }
    .svg-inline--fa,
    .svg-inline--fa * {
      fill: rgba(255,255,255,0.5) !important;
      color: rgba(255,255,255,0.5) !important;
    }
  }

  margin: 0 auto;
  padding: 1.2em;
  border-radius: 0.15em;
`

const OuterWrapper = styled.div`
  max-width: 800px;
  width: 100%;
  @media (max-width: 800px) {
    display: none;
  }
`

const GoLink = styled.a`
  display: inline-flex;
  color: rgba(255,255,255,0.8);
  justify-content: center;
  align-items: center;
  position: relative;
  top: 0.8em;
  right: 0.4em;
  &:after {
    content: '';
    display: inline-block;
    width: 0.65em;
    height: 0.65em;
    border: 1px rgba(255,255,255,0.8) solid;
    border-left: none;
    border-top: none;
    position: relative;
    top: -0.10em;
    margin-left: 0.2em;
    transform: rotate(-45deg);
  }
  &:hover {
    color: ${({theme: {colors}}) => colors.white};
    text-decoration: underline;
  }
`



const Tab = styled.a`
  display: inline-block;
  position: relative;
  height: 1.8em;
  line-height: 1.8em;
  padding: 0 2.4em;
  background: ${({active = false, theme: {colors}}) => active ? colors.white : 'rgba(44, 53, 53, 0.5)'};
  border-right: 2px ${({theme: {colors}}) => colors.white} solid;
  color: ${({theme: {colors}, active = false}) => active ? colors.primary : 'rgba(255,255,255,1)'};
  &:hover {
    color: ${({theme: {colors}, active = false}) => active ? colors.primary : colors.primary};
  }
  &:after {
    content: '';
    display: ${({active = false}) => active ? 'block' : 'none'};
    position: absolute;
    left: calc(50% - 5px);
    bottom: -18px;
    height: 0;
    border: 10px ${({theme: {colors}}) => colors.white} solid;
    border-left: 10px transparent solid;
    border-bottom: 10px transparent solid;
    border-right: 10px transparent solid;
  }
`

const Tabs = styled.span`
  display: inline-flex;
  border: 2px ${({theme: {colors}}) => colors.white} solid;
  border-radius: 0.3em;
  margin-bottom: 8px;
  overflow: hidden;
  ${Tab}:last-child {
    border: none;
  }
`

const SearchFields = styled.div`
  margin: 0;
`
const SearchField = styled.span`
  display: block;
  width: 100%;
  margin: 0.25em;
`


const RadioButton = styled.input`
  appearance: none;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  width: 18px;
  height: 18px;
  border-radius: 18px;
  margin: 0;
  padding: 0;
  border: 1px white solid;
  background: transparent;
  &:active, &:focus {
    outline: none;
  }
  &:checked {
    margin: 0;
    padding: 0;
    outline: none;
    &:before {
      content: '';
      display: block;
      background: white;
      height: 8px;
      width: 8px;
      border-radius: 8px;
    }
  }
`
const RadioLabel = styled.label`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  margin-right: 20px;
  height: 20px;
  line-height: 0;
  cursor: pointer;
  ${RadioButton} {
    margin-right: 10px;
  }
`

const RadioGroup = styled.div`
  display: flex;
  align-items: center;
  justify-content:flex-start;
  margin: 0.75em 0;
`

const ButtonMetaText = styled.span`
  color: rgba(255,255,255,0.5) !important;
`


export default () => (
  <Formik
    onSubmit={() => alert('Go go go!')}
    render={() => (
      <OuterWrapper>
        <Flex justify={'space-between'}>
          <Box>
            <Tabs>
              <Tab active={true}>All</Tab>
              <Tab>New</Tab>
              <Tab>Used</Tab>
            </Tabs>
          </Box>
          <Box>
            <GoLink>Advanced Search</GoLink>
          </Box>
        </Flex>
        <Wrapper>

          <SearchFields>

            <Flex align={'center'}>
              <SearchField>
                <Field
                  type="text"
                  name="Region"
                  placeholder="Region"
                  component={YInput}
                  icons={{left: 'map'}}
                />
              </SearchField>
              <SearchField>
                <Field
                  type="email"
                  name="email"
                  placeholder="Price Range"
                  component={YInput}
                  icons={{left: 'credit-card'}}
                />
              </SearchField>
            </Flex>

            <Flex alignContent={'center'}>
              <SearchField>


                <RadioGroup>
                  <RadioLabel>
                    <RadioButton type="radio" name="type" onChange={() => null} checked={true} />
                    All
                  </RadioLabel>

                  <RadioLabel>
                    <RadioButton type="radio" name="type" />
                    Sail
                  </RadioLabel>
                  <RadioLabel>
                    <RadioButton type="radio" name="type" />
                    Motor
                  </RadioLabel>

                </RadioGroup>
              </SearchField>
              <SearchField>

                <Field
                  type="text"
                  name="model"
                  placeholder="Make or Model"
                  component={YInput}
                  icons={{left: 'wrench'}}
                />

              </SearchField>
            </Flex>



          </SearchFields>
          <Button color={'white'} outline={true} fullwidth={true}>
            <Icon name="search" />
            Search <ButtonMetaText>in 12,741 listings</ButtonMetaText>
          </Button>
        </Wrapper>
      </OuterWrapper>
    )}
  />
)
