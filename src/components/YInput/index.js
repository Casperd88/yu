import React from 'react';
import styled from 'styled-components';
import Icon from './../Icon';

const IconWrapper = styled.span`
  display: inline-block;;
  position: absolute;
  right: 0.5em;
  top: calc(50% - 0.5em);
  width: 1em;
  height: 1em;
  font-size: 1em;
  color: ${({inverse = false, theme: {colors}}) => inverse ? 'rgba(255,255,255,0.8)' : colors.muted};
`;

export const InputComponent = styled.input`
  color: ${({error, theme: {colors}, inverse = false}) => error ? colors.error : colors[inverse ? 'white' : 'text']};
  width: 100%;
  font-size: 1em;
  padding: 0.6em 0;
  background: transparent;
  border: 0;
  font-family: 'Stolzl';
  transition: all 200ms ease-in-out;
  border-bottom: 1px ${({error, inverse = false, theme: {colors}}) => error ? colors.error : colors[inverse ? 'white' : 'muted']} solid;
  &::placeholder {
    color: ${({theme: {colors}, inverse = false}) => inverse ? 'rgba(255,255,255,0.8)' : colors.muted};
  }
  &:active, &:focus, &:hover {
    outline: none;
    border-bottom: 1px ${({error, inverse = false, theme: {colors}}) => error ? colors.error : colors[inverse ? 'white' : 'text']} solid;
  }
`;



const InputWrapper = styled.div`
  display: inline-block;
  line-height: 0;
  width: 100%;
  position: relative;
`;

const Field = styled.div`
  margin-bottom: ${({space = '1em'}) => space};
`

const InputError = styled.p`
  margin: 0.6em 0;
  padding: 0;
  color: ${({theme: {colors}}) => colors.error};
  font-size: 0.85em;
`

const Checkbox = styled.input`
  appearance: none;
  position: relative;
  display: inline-block;
  background-color: ${(props) => props.theme.colors.text};
  cursor: pointer;
  width: 46px;
  height: 26px;
  padding: 3px;
  box-sizing: padding-box;
  overflow: hidden;
  box-shadow: inset 0.1em 0.1em 0.2em rgba(0,0,0,0.2);
  margin: 0;
  margin-right: 10px;
  line-height: 0;
  align-items: center;
  border-radius: 100px;
  transition: background-color 300ms ease-out;

  &:checked {
    background-color: ${(props) => props.theme.colors.primary};
  }

  &:before {
    content: '';
    display: inline-block;
    width: 20px;
    height: 20px;
    background: #fff;
    border-radius: 100px;
    box-shadow: 0.1em 0.1em 0.2em rgba(0,0,0,0.2);
    transition: transform 300ms ease-out;
    transform: translate3d(0,0,0);
  }
  &:checked:before {
    transform: translate3d(20px,0,0);
  }
  &:active, &:focus {
    outline: none;
  }
`;

const YInput = ({
  field,
  space = '1em',
  inverse = false,
  form: {touched, errors},
  icons: {left: iconLeft = false, right: iconRight = false} = {},
  ...props
}) => {

  const showError = touched[field.name] && errors[field.name];

  return props.type === 'checkbox' ? (
    <Checkbox {...field} {...props} />
  ) : (
      <Field space={space}>
        <InputWrapper>
        {iconLeft && (
          <IconWrapper inverse={inverse} error={showError}>
            <Icon name={iconLeft} />
          </IconWrapper>
        )}

          <InputComponent inverse={inverse} hasIcon={true} error={showError} {...field} {...props} />

        {iconRight && (
          <IconWrapper inverse={inverse} error={showError}>
            <Icon name={iconRight} />
          </IconWrapper>
        )}
        </InputWrapper>
        {
          showError &&
          (<InputError>{errors[field.name]}</InputError>)
        }
      </Field>
    );
};

export default YInput;
