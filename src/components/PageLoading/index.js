import React from 'react';
import styled from 'styled-components';
import Spinner from './../../components/Spinner';

const Wrapper = styled.div`
  height: 100vh;
  color: ${({theme: {colors}}) => colors.muted};
  display: flex;
  justify-content: center;
  padding: 10vw;

`
export default () => (
  <Wrapper>
    <Spinner />
  </Wrapper>
);
