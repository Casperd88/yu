import React, { Component } from 'react';
import Aux from 'react-aux';
import { LinkButton } from './../Button';
import UserWidget from './../UserWidget';
import Spinner from './../Spinner';
import Jewel from './../Jewel';
import styled from 'styled-components';
import { lighten } from 'polished';
import { Link, withRouter } from 'react-router-dom';
import { LogoSvg } from './../Logo'

const StylableLink = ({to, children, ...props}) => (<Link to={to} {...props}>{children}</Link>);
const NavLink = styled(StylableLink)`
  text-decoration: none;
`;

const LogoLink = styled(StylableLink)`
  transform: scale(1);
  transition: 250ms transform ease-in;
  margin: 0.15em;
  font-size: 2.6em;
  background: transparent;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  &:active {
    transform: scale(0.9);
  }
  svg {
    width: 1em;
    height: 1em;
    fill: ${({background}) => background === 'transparent' ? 'rgba(255,255,255,0.5)' : lighten(0.25, '#677371')};
    transition: 250ms fill ease-in;
  }
  &:hover svg {
    fill: ${({theme: {colors}}) => colors.primary};
  }
`;

const Actions = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 -1em;
  ${NavLink} {
    color: ${({background, theme: {colors}}) => background === 'transparent' ? '#fff' : colors.text};
    &:hover {
      color: ${({theme: {colors}}) => colors.primary};
    }
  }
`;

const Wrapper = styled.nav`
  padding: ${({background}) => background === 'transparent' ? '1.2em 2.4em' : '0.4em 0.8em'};
  @media (max-width: 800px) {
    padding: 0.75em;
  }
  font-size: 1em;
  background: ${({background}) => background};
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  z-index: 1000;
  box-shadow: ${({background, theme}) => background !== 'transparent' ? theme.boxShadow : 'none'};
  transition: padding 150ms 250ms ease-in, background 300ms ease;
`;

const ListButton = styled(LinkButton)`
  @media (max-width: 600px) {
    padding: 0.8em;
    margin: 0 1em;
    span {
      display: none;
    }
    svg {
      margin: 0;
      padding: 0;
    }
  }
`;

const Box = styled.span`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  margin: 0;
  @media (min-width: 600px) {
    margin: 0 1em;
  }
`
const JewelBox = Box.extend`
`

const EmptyWrapper = styled.div`
  height: 280px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  text-align: center;
  margin: -0.3em -0.5em;
  border-radius: 0.1em;

  color: rgba(0,0,0,.15);
  p {
    color: ${({theme}) => theme.colors.text};
  }
  svg {
    width: 96px;
    height: 96px;
    fill: rgba(0,0,0,.15);
  }
`

class Empty extends Component {

  state = {mounted: false}

  componentDidMount() {
    // Simulate spinner
    setTimeout(() => this.setState({mounted: true}), 600)
  }

  render() {
    return (
      <EmptyWrapper>
        {this.state.mounted ? (
          this.props.children
        ) : (
          <Spinner />
        )}
      </EmptyWrapper>
    )
  }
}

class NavBar extends React.Component {

  state = {userOpen: false}

  componentWillMount() {
    window.addEventListener('scroll', this.updateState.bind(this));
    this.updateState();
  }

  updateState() {
    this.setState({overlay: (document.documentElement.scrollTop || document.body.scrollTop) > 100});
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.updateState.bind(this));
  }

  render() {

    const {location: {pathname}} = this.props;
    const isLight = this.state.overlay || pathname !== '/';
    const bgColor = isLight ? '#fff' : 'transparent';

    return (
      <Wrapper background={bgColor}>
        <LogoLink background={bgColor} to="/"><LogoSvg /></LogoLink>
        <Actions background={bgColor}>
          {this.props.user && (
            <Aux>
              <JewelBox>
                <Jewel isLight={isLight} count={0} Icon={() => (
                  <svg viewBox="0 0 100 100">
                  <path d="M89.582,61.493C93.126,57.215,95,52.148,95,46.845c0-13.796-12.855-25.021-28.657-25.021c-1.464,0-2.954,0.102-4.438,0.304  c-6.186-4.277-13.911-6.63-21.802-6.63C20.746,15.498,5,29.213,5,46.069c0,6.432,2.263,12.578,6.544,17.783l0.067,0.082  c1.024,1.236,1.495,1.715,1.657,1.867c0.657,0.612,1.011,1.756,0.946,2.115L12.02,79.945c-0.458,2.51,0.835,3.66,1.417,4.043  c0.362,0.232,0.964,0.514,1.776,0.514c0.819,0,1.656-0.285,2.486-0.848l11.449-7.729c0.295-0.2,1.018-0.417,1.669-0.417  c0.175,0,0.287,0.018,0.348,0.029c0.049,0.012,4.929,1.098,8.937,1.098c7.142,0,14.009-1.865,19.888-5.4  c2.107,0.418,4.242,0.629,6.354,0.629c3.104,0,6.854-0.799,7.273-0.891c0.014-0.004,0.064-0.014,0.169-0.014  c0.43,0,0.925,0.148,1.078,0.254l9.187,6.203c0.75,0.507,1.512,0.764,2.266,0.764c0.779,0,1.354-0.27,1.699-0.496  c0.424-0.279,1.774-1.371,1.344-3.735l-1.76-9.626c-0.01-0.204,0.217-0.92,0.61-1.285C88.334,62.923,88.714,62.542,89.582,61.493z   M40.103,71.715c-3.219,0-7.407-0.879-7.877-0.979c-1.797-0.395-4.327,0.096-5.833,1.115L17.39,77.93l1.207-6.625l0.457-2.505  c0.391-2.146-0.627-4.916-2.401-6.569c-0.004-0.002-0.322-0.317-1.121-1.274L15.4,60.795c-3.585-4.331-5.479-9.423-5.479-14.725  c0-14.144,13.54-25.65,30.182-25.65s30.18,11.507,30.18,25.65C70.283,60.211,56.745,71.715,40.103,71.715z M85.793,58.355  c-0.604,0.729-0.889,1.03-0.956,1.1c-0.121,0.113-0.237,0.234-0.351,0.357l-0.08,0.082l0.003,0.006  c-1.274,1.467-1.959,3.59-1.652,5.276l1.13,6.191l-6.268-4.232c-0.995-0.674-2.467-1.093-3.84-1.093  c-0.436,0-0.846,0.042-1.23,0.126c-0.371,0.079-3.703,0.772-6.206,0.772c-0.207,0-0.415-0.002-0.625-0.008  c6.054-5.641,9.484-13.135,9.484-20.866c0-7.076-2.795-13.858-7.914-19.312C79.939,27.18,90.08,36.028,90.08,46.845  C90.08,50.99,88.598,54.971,85.793,58.355z"/>
                  </svg>
                )}>
                <Empty>
                  <svg viewBox="0 0 100 100">
                  <path d="M89.582,61.493C93.126,57.215,95,52.148,95,46.845c0-13.796-12.855-25.021-28.657-25.021c-1.464,0-2.954,0.102-4.438,0.304  c-6.186-4.277-13.911-6.63-21.802-6.63C20.746,15.498,5,29.213,5,46.069c0,6.432,2.263,12.578,6.544,17.783l0.067,0.082  c1.024,1.236,1.495,1.715,1.657,1.867c0.657,0.612,1.011,1.756,0.946,2.115L12.02,79.945c-0.458,2.51,0.835,3.66,1.417,4.043  c0.362,0.232,0.964,0.514,1.776,0.514c0.819,0,1.656-0.285,2.486-0.848l11.449-7.729c0.295-0.2,1.018-0.417,1.669-0.417  c0.175,0,0.287,0.018,0.348,0.029c0.049,0.012,4.929,1.098,8.937,1.098c7.142,0,14.009-1.865,19.888-5.4  c2.107,0.418,4.242,0.629,6.354,0.629c3.104,0,6.854-0.799,7.273-0.891c0.014-0.004,0.064-0.014,0.169-0.014  c0.43,0,0.925,0.148,1.078,0.254l9.187,6.203c0.75,0.507,1.512,0.764,2.266,0.764c0.779,0,1.354-0.27,1.699-0.496  c0.424-0.279,1.774-1.371,1.344-3.735l-1.76-9.626c-0.01-0.204,0.217-0.92,0.61-1.285C88.334,62.923,88.714,62.542,89.582,61.493z   M40.103,71.715c-3.219,0-7.407-0.879-7.877-0.979c-1.797-0.395-4.327,0.096-5.833,1.115L17.39,77.93l1.207-6.625l0.457-2.505  c0.391-2.146-0.627-4.916-2.401-6.569c-0.004-0.002-0.322-0.317-1.121-1.274L15.4,60.795c-3.585-4.331-5.479-9.423-5.479-14.725  c0-14.144,13.54-25.65,30.182-25.65s30.18,11.507,30.18,25.65C70.283,60.211,56.745,71.715,40.103,71.715z M85.793,58.355  c-0.604,0.729-0.889,1.03-0.956,1.1c-0.121,0.113-0.237,0.234-0.351,0.357l-0.08,0.082l0.003,0.006  c-1.274,1.467-1.959,3.59-1.652,5.276l1.13,6.191l-6.268-4.232c-0.995-0.674-2.467-1.093-3.84-1.093  c-0.436,0-0.846,0.042-1.23,0.126c-0.371,0.079-3.703,0.772-6.206,0.772c-0.207,0-0.415-0.002-0.625-0.008  c6.054-5.641,9.484-13.135,9.484-20.866c0-7.076-2.795-13.858-7.914-19.312C79.939,27.18,90.08,36.028,90.08,46.845  C90.08,50.99,88.598,54.971,85.793,58.355z"/>
                  </svg>
                  <p>No messages</p>
                </Empty>
                </Jewel>
                <Jewel isLight={isLight} count={3} Icon={() => (
                  <svg viewBox="0 0 100 100">
                    <path d="M84.871,66.409c-1.193-1.366-2.889-3.685-4.189-6.974c-1.291-3.269-1.945-6.863-1.945-10.688  c0-11.371-6.906-21.842-17.281-26.359c0.666-1.548,1.016-3.231,1.016-4.92C62.469,10.594,56.876,5,50,5s-12.469,5.594-12.469,12.469  c0,1.688,0.349,3.372,1.015,4.92c-10.375,4.518-17.281,14.989-17.281,26.359c0,3.824-0.654,7.42-1.945,10.688  c-1.302,3.289-2.997,5.607-4.191,6.976c-1.264,1.444-2.621,4.079-2.621,6.442v3.393c0,3.441,2.776,6.24,6.189,6.24l15.514-0.004  C35.915,89.771,42.462,95,50,95s14.084-5.229,15.789-12.514H81.46c3.326,0,6.032-2.799,6.032-6.24v-3.393  C87.492,70.49,86.135,67.855,84.871,66.409z M82.574,76.246c0,0.729-0.499,1.32-1.112,1.32H65.118c-1.923,0-3.655,1.459-4.031,3.392  C60.053,86.243,55.391,90.08,50,90.08c-5.391,0-10.053-3.836-11.087-9.118c-0.376-1.937-2.109-3.396-4.031-3.396H18.697  c-0.7,0-1.27-0.592-1.27-1.32v-3.393h0.001c0-0.754,0.602-2.287,1.407-3.209c1.449-1.66,3.502-4.463,5.058-8.402  c1.521-3.85,2.293-8.053,2.293-12.494c0-10.082,6.384-19.108,15.887-22.462c0.945-0.335,1.664-1.046,1.97-1.948  c0.312-0.912,0.174-1.925-0.38-2.778c-0.793-1.22-1.211-2.634-1.211-4.091c0-4.162,3.387-7.549,7.549-7.549s7.549,3.387,7.549,7.549  c0,1.454-0.419,2.869-1.211,4.091c-0.554,0.854-0.69,1.866-0.381,2.774c0.308,0.907,1.024,1.618,1.972,1.951  c9.502,3.354,15.887,12.381,15.887,22.463c0,4.441,0.771,8.646,2.293,12.494c1.556,3.939,3.607,6.742,5.057,8.4  c0.807,0.924,1.408,2.457,1.408,3.211V76.246z"/>
                  </svg>
                )}>
                  <Empty>
                    <svg viewBox="0 0 100 100">
                      <path d="M84.871,66.409c-1.193-1.366-2.889-3.685-4.189-6.974c-1.291-3.269-1.945-6.863-1.945-10.688  c0-11.371-6.906-21.842-17.281-26.359c0.666-1.548,1.016-3.231,1.016-4.92C62.469,10.594,56.876,5,50,5s-12.469,5.594-12.469,12.469  c0,1.688,0.349,3.372,1.015,4.92c-10.375,4.518-17.281,14.989-17.281,26.359c0,3.824-0.654,7.42-1.945,10.688  c-1.302,3.289-2.997,5.607-4.191,6.976c-1.264,1.444-2.621,4.079-2.621,6.442v3.393c0,3.441,2.776,6.24,6.189,6.24l15.514-0.004  C35.915,89.771,42.462,95,50,95s14.084-5.229,15.789-12.514H81.46c3.326,0,6.032-2.799,6.032-6.24v-3.393  C87.492,70.49,86.135,67.855,84.871,66.409z M82.574,76.246c0,0.729-0.499,1.32-1.112,1.32H65.118c-1.923,0-3.655,1.459-4.031,3.392  C60.053,86.243,55.391,90.08,50,90.08c-5.391,0-10.053-3.836-11.087-9.118c-0.376-1.937-2.109-3.396-4.031-3.396H18.697  c-0.7,0-1.27-0.592-1.27-1.32v-3.393h0.001c0-0.754,0.602-2.287,1.407-3.209c1.449-1.66,3.502-4.463,5.058-8.402  c1.521-3.85,2.293-8.053,2.293-12.494c0-10.082,6.384-19.108,15.887-22.462c0.945-0.335,1.664-1.046,1.97-1.948  c0.312-0.912,0.174-1.925-0.38-2.778c-0.793-1.22-1.211-2.634-1.211-4.091c0-4.162,3.387-7.549,7.549-7.549s7.549,3.387,7.549,7.549  c0,1.454-0.419,2.869-1.211,4.091c-0.554,0.854-0.69,1.866-0.381,2.774c0.308,0.907,1.024,1.618,1.972,1.951  c9.502,3.354,15.887,12.381,15.887,22.463c0,4.441,0.771,8.646,2.293,12.494c1.556,3.939,3.607,6.742,5.057,8.4  c0.807,0.924,1.408,2.457,1.408,3.211V76.246z"/>
                    </svg>
                    <p>No notifications</p>

                  </Empty>
                </Jewel>
              </JewelBox>
              <UserWidget isDark={!isLight} name={this.props.user.email.split('@')[0]} open={this.state.userOpen} onClick={() => this.setState({userOpen: !this.state.userOpen})} />
            </Aux>
          )}
          {!this.props.user && (
            <Aux>
              <Box><NavLink to="/login">Log in</NavLink></Box>
              <Box><NavLink to="/signup">Sign up</NavLink></Box>
            </Aux>
          )}
          <Box>
            <ListButton to="/list" color={'secondary'} Icon={LogoSvg}>Sell my boat</ListButton>
          </Box>
        </Actions>
      </Wrapper>
    );
  }
}

export default withRouter(NavBar);
