import React from 'react';
import styled from 'styled-components';
import Icon from './../Icon';

const IconWrapper = styled.span`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  right: 0;
  top: calc(50% - 0.6em);
  height: 1.2em;
  width: 1.2em;
  font-size: 1em;
  transition: all 200ms ease;
  color: ${({theme: {colors}, inverse}) => inverse ? 'rgba(255,255,255,0.8)' : colors.muted};
`

const Wrapper = styled.span`
  display: inline-block;
  line-height: 0;
  position: relative;
  margin-bottom: ${({space = '1.5em'}) => space};
  border-bottom: 1px ${({inverse, theme: {colors}}) => inverse ? colors.white : colors.muted} solid;
  transition: all 200ms ease;
  width: 100%;
  &:hover {
    border-bottom: 1px ${({theme: {colors}, inverse}) => inverse ? colors.white : colors.text} solid;
    ${IconWrapper} {
      color: ${({theme: {colors}, inverse}) => inverse ? colors.white : colors.text};
    }
  }
`;

const Select = styled.select`
  appearance: none;
  border: none;
  background: transparent;
  display: inline-block;
  font-size: 1em;
  font-family: 'Stolzl', sans-serif;
  color: ${({theme: {colors}, inverse}) => inverse ? colors.white : colors.text};
  padding: 0.6em 0;
  background: ${({theme: {colors}, inverse}) => inverse ? colors.primary : colors.white};
  margin: 0;
  position: relative;
  width: 100%;
  &:focus, &:active {
    outline: none;
  }
`;

export default ({children, space = '1.5em', inverse = false, ...props}) => (
  <Wrapper inverse={inverse} space={space}>
    <Select inverse={inverse} {...props}>{children}</Select>
    <IconWrapper inverse={inverse}>
      <Icon name={'angle-down'} />
    </IconWrapper>
  </Wrapper>
);
