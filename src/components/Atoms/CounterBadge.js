import styled from 'styled-components'

export default styled.span`
  display: inline-block;
  background: ${({color = 'text', theme: {colors}}) => colors[color]};
  color: ${({theme: {colors}}) => colors.white};
  font-size: 0.75em;
  line-height: 1.4em;
  text-align: center;
  padding: 0.3em 0.6em;
  border-radius: 0.2em;
  font-weight: bold;
`
