import styled from 'styled-components'

export const Nav = styled.nav`
  display: block;
`

export const Link = styled.a`
  display: block;
  font-size: 1em;
  line-height: calc(1em + 2px);
  padding: 0.5em 0;
  color: ${({theme: {colors}}) => colors.text};
  &:hover {
    color: ${({theme: {colors}}) => colors.bold};
  }
`

export const Rule = styled.hr`
  display: block;
  margin: 0.5em 0;
  padding: 0;
  height: 1px;
  border: none;
  background: ${({theme: {colors}}) => colors.muted};
  width: 100%;
`
