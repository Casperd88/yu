import styled from 'styled-components';
import React from 'react';
import { LogoSvg } from './../Logo';
import TextDivider from './../TextDivider';

const LogoWrapper = styled.span`
  display: block;
  width: 2em;
  height: 2em;
  color: ${({theme: {colors}}) => colors.muted};
  svg {
    height: 2em;
    width: 2em;
    fill: currentColor;
  }
`

export const Hr = () => (
  <TextDivider color={'muted'}><LogoWrapper><LogoSvg /></LogoWrapper></TextDivider>
)

export const Spacer = styled.div`
  margin-bottom: 1.5em;
`

export const TextCenter = styled.div`
  text-align: center;
`
export const SmallText = styled.div`
  font-size: 0.8em;
  color: ${({theme: {colors}}) => colors.text};
`
