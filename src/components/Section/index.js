import styled from 'styled-components';

export const Section = styled.div`
  margin: 2em 4em 4em 4em;
  @media (max-width: 800px) {
    margin: 1em 2em 2em 2em;
  }
`;

export const Title = styled.h2`
  margin: 2.4em 0 1.2em 0;
  font-size: 1.2em;
  @media (max-width: 800px) {
    margin: 1.6em 0 0.8em 0;
  }
  color: ${({theme: {colors}}) => colors.bold};
`;

export const FormSection = Section.extend`
  background: #fff;
  margin: 0;
  padding: 1.5em;

  @media (min-width: 600px) {
    padding: 2.5em;
    margin: 4em auto;
    max-width: 500px;
  }
  @media (max-width: 600px) {
    margin: 0;
    border-bottom: 1px ${({theme: {colors}}) => colors.muted} solid;
  }
  box-shadow: ${({theme}) => theme.boxShadow};
  ${Title} {
    font-size: 1.4em;
    text-align: center;
    margin: 0 0 1em 0;
    color: ${({theme: {colors}}) => colors.bold};
  }
`;

export const ContentSection = FormSection.extend`
  text-align: justify;
  @media (min-width: 600px) {
    max-width: 650px;
  }
`
