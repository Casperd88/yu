import styled from 'styled-components';

const CircleButton = styled.a`
  height: 1.6em;
  width: 1.6em;
  border-radius: 1.6em;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  background: white;
  overflow: hidden;
  box-shadow: 0 1px 3px rgba(0,0,0,0.3);
  font-size: 2em;
  color: #9ba7a5;
  transform: scale(1);
  transition: all 150ms ease;
  &:hover {
    color: #677371;
  }
  &:active {
    transform: scale(0.9);
  }
`;

export default CircleButton;
