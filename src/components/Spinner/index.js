import styled, {injectGlobal} from 'styled-components';

injectGlobal`
  @keyframes spinner {
    0%,
    80%,
    100% {
      box-shadow: 0 2.5em 0 -1.3em;
    }
    40% {
      box-shadow: 0 2.5em 0 0;
    }
  }
`;

const Spinner = styled.span`
  display: inline-block;
  border-radius: 50%;
  width: 2.5em;
  height: 2.5em;
  animation-fill-mode: both;
  font-size: 0.5em;
  animation: spinner 1.8s infinite ease-in-out;
  &:before, &:after {
    content: '';
    position: absolute;
    top: 0;
    border-radius: 50%;
    width: 2.5em;
    height: 2.5em;
    animation-fill-mode: both;
    animation: spinner 1.8s infinite ease-in-out;
  }
  &:before {
    left: -3.5em;
    animation-delay: -0.32s;
  }
  &:after {
    left: 3.5em;
  }
  font-size: 5px;
  margin: auto;
  margin-top: calc(-2.5em + 2px);
  margin-bottom: calc(2.5em + 2px);
  position: relative;
  text-indent: -9999em;
  transform: translateZ(0);
  animation-delay: -0.16s;
`;

export default Spinner;
