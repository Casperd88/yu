import styled from 'styled-components';

export default styled.span`
  display: flex;
  align-items: center;
  height: 2em;
  justify-content: center;
  align-items: center;
  line-height: 2em;
  margin: 1.5em 0;
  color: ${({theme: {colors}, color = 'text'}) => colors[color]};
  &:before, &:after {
    display: inline-block;
    content: '';
    height: 1px;
    width: 100%;
    background: ${({theme: {colors}, color = 'text'}) => colors[color]};
  }
  &:before {
    margin-right: 1em;
  }
  &:after {
    margin-left: 1em;
  }
`;
