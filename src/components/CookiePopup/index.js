import styled from 'styled-components'
import React, {Component} from 'react'
import Button from './../Button'
import { Link } from 'react-router-dom'
import Cookies from 'universal-cookie'

const Wrapper = styled.div`
  display: block;
  padding: 1.4em;
  width: 100%;
  position: fixed;
  bottom: 0;
  left: 0;
  z-index: 1000;
  font-size: 1em;
  background: rgba(44, 53, 53, 0.95);
  color: ${({theme: {colors}}) => colors.lightgrey};
  p {
    line-height: 24px;
    margin: 0;
    padding: 0;
    font-size: 14px;
  }
`
const Title = styled.h2`
  margin: 0;
  padding: 0;
  font-size: 22px;
  font-weight: 400;
  color: ${({theme: {colors}}) => colors.white};
  margin-bottom: 0.3em;
`
const Flex = styled.div`
  display: flex;
  align-items: flex-end;
  flex-direction: column;
  > p {
    margin-bottom: 1.5em;
  }
  > div {
    padding-left:1.5em;
  }
  @media (min-width: 600px) {
    flex-direction: row;
    > p {
      margin-bottom: 0;
    }
  }

`;

const cookies = new Cookies();
const COOKIE_ID = 'cookies_accepted';

class CookiePopup extends Component {

  state = {closed: !!cookies.get(COOKIE_ID)}

  close() {
    const onYearFromNow = new Date(new Date().setFullYear(new Date().getFullYear() + 1));
    cookies.set(COOKIE_ID, true, {expires: onYearFromNow});
    this.setState({closed: true});
  }

  widget() {
    return (
      <Wrapper>
        <Title>
          Cookie Policy
        </Title>
        <Flex>
          <p>
            Our site uses cookies to help you enjoy the best possible experience. By carrying on browsing this site, you give the thumbs-up for cookies to be used. For more details about them, including how you can amend your preferences, please read our <Link to="/cookie-policy">Cookie Policy</Link>.
          </p>
          <div>
            <Button onClick={this.close.bind(this)} outline={true} color={'white'}>Close</Button>
          </div>
        </Flex>
      </Wrapper>
    );
  }

  render() {
    return this.state.closed ? null : this.widget();
  }
}



export default CookiePopup;
