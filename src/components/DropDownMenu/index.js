import React, { Component } from 'react'
import styled from 'styled-components'
import Transition from 'react-transition-group/Transition'
import onClickOutside from 'react-onclickoutside'

const Menu = styled.nav`
  display: block;
  width: calc(100vw - 2em);
  left: 1em;
  @media (min-width: 600px) {
    width: 250px;
    left: calc(50% - 125px);
  }
  background: ${({theme: {colors}}) => colors.white};
  position: absolute;
  border: 1px rgba(0,0,0,0.2) solid;
  box-shadow: ${({theme}) => theme.boxShadowLarge};
  border-radius: 0.3em;
  padding: 0.8em 1em;

  top: calc(100% + 5px);

  &:after {
    display: none;
    @media (min-width: 600px) {
      display: block;
    }
    content: '';
    width: 8px;
    height: 8px;
    position: absolute;
    top: -5px;
    left: calc(50% - 5px);
    background: ${({theme: {colors}}) => colors.white};
    border-top: 1px rgba(0,0,0,0.2) solid;
    border-left: 1px rgba(0,0,0,0.2) solid;
    transform: rotate(45deg);
  }
  hr {
    display: block;
    margin: 0.5em 0;
    padding: 0;
    height: 1px;
    border: none;
    background: ${({theme: {colors}}) => colors.muted};
    width: 100%;
  }
  ul {
    list-style: none;
    margin: 0;
    padding: 0;
    li {
      list-style: none;
      margin: 0;
      padding: 0;
      line-height: 2em;
      a {
        display: inline-block;
        width: 100%;
        color: ${({theme: {colors}}) => colors.text};
        &:hover {
          color: ${({theme: {colors}}) => colors.bold};
        }
        text-decoration: none;
      }
    }
  }
`
const transform = 'translate3d(0, -10px, 0)'

const defaultStyle = {
  transition: `all 200ms ease-in-out`,
  opacity: 0,
  pointerEvents: 'none',
  transform
}

const transitionStyle = {
  entering: {opacity: 0, transform},
  entered: {opacity: 1, pointerEvents: 'auto', transform: 'translate3d(0, 0, 0)'}
}

const Container = styled.div`
  display: inline-block;
`

const ToggleWrapper = styled.div`
  position: absolute;
  left: 0;
  @media (min-width: 600px) {
    position: relative;
  }

`

const ButtonWrapper = styled.div`
  display: inline-flex;
`

class DropDownMenu extends Component {

  state = {open: false}

  toggle() {
    this.setState({open: !this.state.open})
  }

  handleClickOutside() {
    this.setState({open: false})
  }

  render() {
    const {ToggleButton, children, ...props} = this.props;
    return (
      <Container>
        <ButtonWrapper>
          <ToggleButton
            open={this.state.open}
            toggle={this.toggle.bind(this)}
          />
        </ButtonWrapper>



        <Transition in={this.state.open} timeout={0}>
        {(state) => (
          <ToggleWrapper style={{
            ...defaultStyle,
            ...transitionStyle[state]
          }} {...props}>
            <Menu>
              {this.state.open && children}
            </Menu>
          </ToggleWrapper>
        )}
        </Transition>

      </Container>
    )
  }
}

export default onClickOutside(DropDownMenu);
