import React from 'react'
import styled from 'styled-components'
import { Link } from './../Atoms/Nav'
import CounterBadge from './../Atoms/CounterBadge'

const CounterLinkComponent = ({count = false, children, ...props}) => {
  return (
    <Link {...props}>
      {children}
      {count !== false && (
        <CounterBadge>{count}</CounterBadge>
      )}
    </Link>
  )
}

export const CounterLink = styled(CounterLinkComponent)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  ${CounterBadge} {
    opacity: 0.5;
    transition: opacity 200ms ease-in-out;
  }
  &:hover ${CounterBadge} {
    opacity: 1;
  }
`
