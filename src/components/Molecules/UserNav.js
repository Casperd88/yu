import React from 'react'
import Aux from 'react-aux'
import { Nav, Link, Rule } from './../Atoms/Nav'
import { CounterLink } from './CounterLink'

export const UserNav = ({
  logoutAction = () => null,
  listCount = 0,
  likeCount = 129
}) => (
  <Aux>
    <Nav>
      <CounterLink href="/" count={listCount}>My Listings</CounterLink>
      <CounterLink href="/" count={likeCount}>Liked</CounterLink>
      <Link href="/">Account Settings</Link>
    </Nav>
    <Rule />
    <Nav>
      <Link onClick={logoutAction}>
        Log Out
      </Link>
    </Nav>
  </Aux>
)
