import React, { Component } from 'react';
import styled from 'styled-components';
import CircleButton from './../CircleButton';
import Icon from './../Icon';


const CarouselCircleButton = CircleButton.extend`
  position: absolute;
  top: calc(50% - 0.8em);
  z-index: 1;
`;

const CarouselCircleButtonPrevious = CarouselCircleButton.extend`
  left: -0.8em;
`;

const CarouselCircleButtonNext = CarouselCircleButton.extend`
  right: -0.8em;
`;

const CarouselPhotoButtonNext = styled.a`
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 10;
  background: rgba(62, 74, 74, 0.75);
  border-radius: 0;
  width: 0.50em;
  right: 0.25em;
  height: 4em;
  top: calc(50% - 2em);
  border-radius: 0.125em;
  box-shadow: none;
  color: rgba(255,255,255,0.5);
  font-size: 2em;
  opacity: 0;
  transition: opacity 200ms ease;
  &:active {
    background: rgba(62, 74, 74, 1);
  }
  svg {
    width: 0.6em;
    height: 0.6em;
  }
  &:hover {
    color: white;
    .icon {
      opacity: 1;
    }
  }
`;

const CarouselPhotoButtonPrevious = CarouselPhotoButtonNext.extend`
  right: auto;
  left: 0.25em;
`;

const CarouselWrapper = styled.div`
  position: relative;
  user-select: none;
`;

const CarouselContainer = styled.div`
  white-space: nowrap;
  position: relative;
  overflow: hidden;
  margin: ${({space}) => -space}em;
`;

const CarouselPanel = styled.div`
  display: block;
  white-space: nowrap;
  transform: translate3d(0,0,0);
  transition: transform 200ms ease-in;
`;

const CarouselItem = styled.div`
  display: inline-block;
  padding: ${({space}) => space}em;
  width: calc(100% / ${({items}) => items.mobile});
  @media (min-width: 600px) {
    width: calc(100% / ${({items}) => items.landscape});
  }
  @media (min-width: 800px) {
    width: calc(100% / ${({items}) => items.tablet});
  }
  @media (min-width: 1200px) {
    width: calc(100% / ${({items}) => items.desktop});
  }
  &:hover ${CarouselPhotoButtonNext},
  &:hover ${CarouselPhotoButtonPrevious} {
    opacity: 1;
  }
`;

const ScreenType = () => {
  const width = window.innerWidth;
  if ( width < 600) return 'mobile';
  else if (width < 800) return 'landscape';
  else if (width < 1200) return 'tablet';
  else return 'desktop';
}


class Carousel extends Component {

  componentWillMount() {
    this.initialize();
  }

  componentWillReceiveProps(nextProps) {
    this.initialize();
  }

  initialize() {
    this.setState({position: 0});
  }

  next() {
    this.setState({position: this.state.position + 1});
  }

  previous() {
    this.setState({position: this.state.position - 1});
  }

  render() {

    const {children, items, space, photos} = this.props;

    const numItems = items[ScreenType()];

    const ButtonPrevious = photos ?
      CarouselPhotoButtonPrevious :
      CarouselCircleButtonPrevious;
    const ButtonNext = photos ?
      CarouselPhotoButtonNext :
      CarouselCircleButtonNext;

    return (
      <CarouselWrapper space={space}>
        {this.state.position > 0 && (
          <ButtonPrevious onClick={this.previous.bind(this)}>
            <Icon name="angle-left" />
          </ButtonPrevious>
        )}
        {
          (children.length - numItems > this.state.position) && (
            <ButtonNext onClick={this.next.bind(this)}>
              <Icon name="angle-right" />
            </ButtonNext>
          )
        }
        <CarouselContainer space={space}>
          <CarouselPanel style={{
            transform: `translate3d(-${100 / numItems * this.state.position}%,0,0)`
          }}>
            {children.map((child, index) => (
              <CarouselItem key={index} space={space} items={items}>
                {child}
              </CarouselItem>
            ))}
          </CarouselPanel>
        </CarouselContainer>
      </CarouselWrapper>
    );
  }
}

export default Carousel;
