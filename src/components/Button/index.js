import React from 'react';
import styled from 'styled-components';
import Aux from 'react-aux';
import { lighten, darken } from 'polished';
import { Link } from 'react-router-dom';
import Spinner from './../Spinner';

const Label = styled.span`
  display: inline-block;
`
const Button = styled.button`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  font-family: 'Stolzl';
  cursor: pointer;
  font-size: ${({size = '1em'}) => size};
  line-height: 1em;
  background-color: ${({theme: {colors}, color = 'primary', outline = false}) => outline ? 'transparent' : colors[color]};
  padding: 0.7em 0.7em;
  color: ${({theme: {colors}, color = 'primary', outline = false}) => !outline ? color === 'white' ? colors.primary : '#fff' : colors[color]};
  border-radius: 0.25em;
  border: 2px ${({theme: {colors}, color = 'primary', outline = false}) => outline ? colors[color] : 'transparent'} solid;
  vertical-align: bottom;
  width: ${({fullwidth}) => fullwidth ? '100%' : 'auto'};
  &[disabled] {
    opacity: 0.5;
  }
  > span {
    position: relative;
    top: 0.05em;
  }
  &:hover {
    color: white;
    background-color: ${({theme: {colors}, color = 'primary'}) => color !== 'white' ? lighten(0.05, colors[color]) : 'rgba(255,255,255,.2)'};
  }
  &:active, &:focus {
    color: white;
    outline: none;
    background-color: ${({theme: {colors}, color = 'primary'}) => color !== 'white' ? darken(0.05, colors[color]) : 'rgba(0,0,0,.2)'};
    box-shadow: inset 0 1px 3px rgba(0,0,0,0.2);
  }
  svg {
    height: 1.2em;
    width: 1.2em;
    margin: 0 0.4em;
    fill: #fff;
    vertical-align: bottom;
  }
  ${Label} {
    margin: 0 0.4em;
  }
`;

const RouterLink = Button.withComponent(Link);

export default ({children, loading, Icon, ...props}) => (
  <Button {...props} disabled={loading}>
    {loading && (
      <Spinner />
    )}
    {!loading && (
      <Aux>
        {Icon && <Icon />}
        <Label>{children}</Label>
      </Aux>
    )}

  </Button>
);

export const LinkButton = ({children, loading, to, Icon, ...props}) => (
  <RouterLink to={to} {...props}>
    {Icon && <Icon />}
    <Label>{children}</Label>
  </RouterLink>
);
