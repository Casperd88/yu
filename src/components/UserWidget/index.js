import React from 'react'
import {connect} from 'react-redux'
import { withRouter } from 'react-router-dom'
import styled from 'styled-components'
import Icon from './../Icon'
import {authLogout} from './../../actions/auth'
import theme from './../../theme'
import DropDownMenu from './../DropDownMenu'
import { UserNav } from './../Molecules/UserNav'


const Label = styled.span`
  display: none;
  @media (min-width: 600px) {
    transition: color 200ms ease;
    display: inline-block;
  }
`

const Divider = styled.span`
  display: none;
  height: 1.5em;
  width: 1px;
  background: ${({color}) => color};
  margin-left: 0.8em;
  @media (min-width: 600px) {
    display: inline-block;
  }

`
const IconWrapper = styled.span`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  width: 0.6em;
  height: 0;
  position: relative;
  top: -0.1em;
  margin: 0 0.3em;
  transform: ${({open}) => open ? 'rotate(180deg)' : 'rotate(0)'};
  transition: transform 300ms ease-in-out;
  @media (min-width: 600px) {
    margin: 0 0.6em;
  }
`
const UserIcon = styled.span`
  display: inline-block;
  height: 40px;
  line-height: 43px;
  text-align: center;
  overflow: hidden;
  width: 40px;
  border-radius: 40px;
  margin: 0 0.3em;
  @media (min-width: 800px) {
    margin: 0 0.6em;
  }
  text-decoration: none;
  transition: all 200ms ease;
`

const Wrapper = styled.a`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  user-select: none;
  transition: opacity 200ms ease;
  &:active, &:focus {
    opacity: 0.5;
  }
  &:hover ${IconWrapper} {
    color: ${({theme: {colors}}) => colors.white};
  }
  &:hover ${Label} {
    color: ${({theme: {colors}}) => colors.white};
  }
  &:hover ${UserIcon} {
    color: ${({theme: {colors}}) => colors.primary};
  }
`

const UserWidget = ({history, isDark = false, name, logout, ...props}) => {

  const color = isDark ? 'rgba(255,255,255,0.5)' : theme.colors.muted

  return (
    <DropDownMenu ToggleButton={({open, toggle}) => (
      <Wrapper onClick={toggle}>
        <UserIcon style={{
          backgroundColor: (isDark ? 'rgba(44,53,53,0.5)' : theme.colors.muted),
          color: (isDark ? 'rgba(255,255,255,0.5)' : 'rgba(255,255,255,0.5)')
        }}>
          <Icon name={'user'} />
        </UserIcon>
        <Label style={{
          color: (isDark ? theme.colors.white : theme.colors.bold)
        }}>{name}</Label>
        <Divider color={color} />
        <IconWrapper style={{color}} open={open}><Icon name={'angle-down'} /></IconWrapper>
      </Wrapper>
    )}>
      <UserNav logoutAction={() => logout().then(() => history.push('/login'))} />
    </DropDownMenu>
  )
}
const mapToProps = null
const mapToDispatch = (dispatch) => ({
  logout: () => dispatch(authLogout())
})
export default withRouter(connect(
  mapToProps,
  mapToDispatch
)(UserWidget))
