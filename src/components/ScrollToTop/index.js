import { Component } from 'react';

class ScrollToTop extends Component {

  componentDidMount() {
    document.documentElement.scrollTop = 0;
  }

  render() {
    const {children} = this.props;
    return children;
  }
}

export default ScrollToTop;
