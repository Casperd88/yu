import React, { Component } from 'react';
import firebase from 'firebase';
import { connect } from 'react-redux';
import { authSuccess, authLogoutSuccess } from './../../actions/auth';
import PageLoading from './../PageLoading';

class AuthManager extends Component {

  state = {
    initialized: false
  };

  componentDidMount() {

    this.removeAuthObserver = firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        this.props.login(user);
      } else {
        this.props.logout();
      }
      if (!this.state.initialized) {
        this.setState({initialized: true});
      }
    }.bind(this));
  }

  componentWillUnmount () {
    this.removeAuthObserver();
  }

  render() {

    if (!this.state.initialized) {
      return <PageLoading />;
    } else {
      return this.props.children;
    }
  }
}

const mapStateToProps = null;

const mapDispatchToProps = (dispatch) => ({
  login(user) {
    dispatch(authSuccess(user))
  },
  logout() {
    dispatch(authLogoutSuccess())
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthManager);
