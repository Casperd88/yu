import React, { Component } from 'react';
import styled from 'styled-components';

const LoveWrapper = styled.a`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  width: 2.8em;
  height: 2.8em;
  z-index: 100;
`;

const LoveSvg = styled.svg`
  height: 2.8em;
  width: 2.8em;
  fill: ${({loved}) => loved ? 'rgb(239, 68, 101)' : 'rgba(62, 74, 74, 0.75)'};
  stroke-width: 0.06em;
  stroke: white;
`;

class LoveButton extends Component {

  componentWillMount() {
    this.setState({loved: !!this.props.loved});
  }

  toggle() {
    this.setState({loved: !this.state.loved});
  }

  render() {
    return (
      <LoveWrapper className={this.props.className} onClick={this.toggle.bind(this)}>
        <LoveSvg viewBox="0 0 30 30" loved={this.state.loved}>
          <g>
            <g transform="translate(-180.000000, -225.000000)">
              <path d="M186.382947,240.266912 L194.647258,248.531223 C194.84252,248.726485 195.159102,248.726485 195.354364,248.531223 L203.618675,240.266912 C205.888597,237.99699 205.888597,234.316719 203.618675,232.046796 C201.348752,229.776874 197.668481,229.776874 195.398559,232.046796 C195.083897,232.361457 195.000811,232.499512 195.000811,232.499512 C195.000811,232.499512 194.750842,232.194575 194.603063,232.046796 C192.333141,229.776874 188.65287,229.776874 186.382947,232.046796 C184.113025,234.316719 184.113025,237.99699 186.382947,240.266912 Z"
              />
            </g>
          </g>
        </LoveSvg>
      </LoveWrapper>
    )
  }
};

export default LoveButton;
