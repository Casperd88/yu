import { combineReducers } from 'redux';
import authReducer from './authReducer.js';
import notificationReducer from './notificationReducer.js';

export default combineReducers({
  auth: authReducer,
  notifications: notificationReducer
});
