const defaultState = [];

export default (state = defaultState, action) => {
  switch (action.type) {
    case 'NOTIFY':
      return [...state, action.payload];
      //return [action.payload];
    case 'PURGE_NOTIFY':
      return [];
    case 'REMOVE_NOTIFY':
      return [...(state.filter((item) => item.$id !== action.payload))]
    default:
      return state;
  }
}
