const defaultState = {
  user: false
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case 'AUTH_SUCCESS':
      return {state, user: action.payload};
    case 'AUTH_LOGOUT':
      return {...state, user: false};
    default:
      return state;
  }
}
