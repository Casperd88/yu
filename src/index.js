import React from 'react';
import ReactDOM from 'react-dom';
import Root from './Root';
import registerServiceWorker from './registerServiceWorker';
import firebase from 'firebase';
import './globalStyles.js';

firebase.initializeApp({
  apiKey: "AIzaSyB0FfyuNnc3heCsqh8rSWGmo6CcEJkYbM4",
  authDomain: "yuapp-879f3.firebaseapp.com",
  databaseURL: "https://yuapp-879f3.firebaseio.com",
  projectId: "yuapp-879f3",
  storageBucket: "yuapp-879f3.appspot.com",
  messagingSenderId: "854726541991"
});

ReactDOM.render(<Root />, document.getElementById('root'));
registerServiceWorker();
