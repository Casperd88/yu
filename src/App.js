import React from 'react';
import { connect } from 'react-redux';
import Aux from 'react-aux';
import { BrowserRouter } from 'react-router-dom';
import NavBar from './components/NavBar';
import Footer from './components/Footer';
import CookiePopup from './components/CookiePopup';
import Notifications from './components/Notifications';
import Locale from './components/Locale';
import Routing from './Routing';

const enhance = connect(({auth: {user}}) => ({user}));

export default enhance(({user}) => (
  <BrowserRouter>
    <Aux>
      <Notifications />
      <Locale />
      <NavBar user={user} />
      <Routing />
      <Footer />
      <CookiePopup />
    </Aux>
  </BrowserRouter>
));
