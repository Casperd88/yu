import React from 'react';
import { Route, Switch } from 'react-router-dom';
import createLazyContainer from 'react-lazy-import';
import ScrollToTop from './components/ScrollToTop';
import PageLoading from './components/PageLoading';



const Error = () => <div>Error!</div>;

const page = (Component) => {
  return () => (
    <ScrollToTop>
      <Component />
    </ScrollToTop>
  )
}

const Home = page(createLazyContainer(() => import('./pages/home'), PageLoading, Error));
const Login = page(createLazyContainer(() => import('./pages/login'), PageLoading, Error));
const Signup = page(createLazyContainer(() => import('./pages/signup'), PageLoading, Error));
const ResetPassword = page(createLazyContainer(() => import('./pages/recoverPassword'), PageLoading, Error));
const List = page(createLazyContainer(() => import('./pages/list'), PageLoading, Error));
const Cookies = page(createLazyContainer(() => import('./pages/cookies'), PageLoading, Error));
const Demo = page(createLazyContainer(() => import('./pages/demo'), PageLoading, Error));



const Routing = ({location}) => (
  <Switch location={location}>
    <Route exact path="/" component={Home}/>
    <Route path="/login" component={Login}/>
    <Route path="/signup" component={Signup}/>
    <Route path="/list" component={List}/>
    <Route path="/cookie-policy" component={Cookies}/>
    <Route path="/demo" component={Demo}/>
    <Route path="/recover-password" component={ResetPassword}/>
  </Switch>
);

export default Routing;
